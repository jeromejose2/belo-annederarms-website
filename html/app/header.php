<!DOCTYPE html>
<!--[if IE 8]><html class="ie8"><![endif]-->
<!--[if IE 9]><html class="ie9"><![endif]-->
<!--[if gt IE 9]><!--><html><!--<![endif]-->
<html lang="en">
<head>
    <title>Belo Essentials</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Belo Essentials" />
    <meta name="keywords" content="belo essentials, essentials product, beauty product, cosmetic, body products, face product" />
    <link rel="stylesheet" href="assets/vendors/font-awesome/css/font-awesome.min.css">
    <!--[if lt IE 9]><script src="js/vendor/html5.js"></script><![endif]-->
    <!-- build:css assets/css/main.min.css -->
        <link href="assets/css/main.css" rel="stylesheet">
    <!-- /build -->
</head>

<body>

<header class="curvy"></header>
<header class="main">
    <div class="container">
        <h1 class="logo from-sprites"><a href="#">Belo Essentials Get Beautyful #ANNEderarms</a></h1>
        <a href="#" id="mininav">Mini Nav</a>
        <nav class="navigation">
            <a href="#home" >Home</a>
            <a href="#mechanics"><span>Mechanics</span></a>
            <span class="logo-space">Logo Space</span>
            <a href="#prizes">Prizes</a>
            <a href="#product"><span>Products</span></a>
        </nav>
        <div class="socials">
            <span>Beauty loves company,<br>so follow us online!</span>
            <a href="#" class="fb from-sprites">Facebook</a>
            <a href="#" class="tw from-sprites">Twitter</a>
            <a href="#" class="in from-sprites">Instagram</a>
        </div>
    </div>
    <button class="see-video">
        CLICK HERE TO VIEW VIDEO
    </button>
</header>

