<footer class="main">
    <div class="container">
        <div class="left clearfix">
            <h1 class="logo from-sprites"><a href="#">Belo Essentials Get Beautyful #ANNEderarms</a></h1>

            <div class="footnav">
                <a href="#">HOME</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="#">PRIZES</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="#">MECHANICS</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="#">PRODUCTS</a>
                <p class="copyright">Copyright © 2015 Belo Essentials. All rights reserved.</p>
            </div>

            <p>
                <a href="#" class="share-fb from-sprites">Share with Facebook</a>
                <a href="#" class="share-tw from-sprites">Share with Twitter</a>
            </p>
        </div>
        <div class="right clearfix">
            <figure class="footer-product">Belo Deo</figure>
        </div>
    </div>
</footer><!-- .main footer -->

<script>
    window.jQuery || document.write('<script src="assets/vendors/jquery-1.10.2.min.js"><\/script>')
</script>
<!--[if gt IE 8]><!-->
<!--<![endif]-->
<script type="text/javascript" src="assets/vendors/jquery.fitvids.js"></script>
<script type="text/javascript" src="assets/vendors/jquery-ui.js"></script>
<script type="text/javascript" src="assets/vendors/jquery.slimscroll.min.js"></script>
<!-- build:js assets/js/main.min.js -->
    <script src="assets/js/main.js"></script>
<!-- /build -->

 <script>
 !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");

$(function(){
    /*var scrWidth = $(window).width();
    var slimheight = '550px';
    if(scrWidth <= 767){
         slimheight = '700px';
    }

    console.log(slimheight);*/
    $('.tweet-feed .people-lists').slimScroll({
            height:'auto',
            alwaysVisible: true
    });
   $('.tweet-feed ul li').each(function(){

             $(this).on('click',function(){
                var isLoaded =  $('.picture-cont').hasClass('active');
                var imgSrc = $(this).data('large');
                var  username = $(this).data('username');
                var  fullname = $(this).data('fullname');
                var desc = $(this).data('desc')
                var el = $('.picture-cont');
                el.css({
                    'background-image':'url('+imgSrc+') '
                })
                $('span.username').html(' <i class="fa fa-instagram"></i> '+ username +'');
                $('span.fullname').html(fullname);
                $('span.desc').html(desc);

                if(isLoaded){
                     el.removeClass('active');
                     setTimeout(function(){
                         el.addClass('active');
                     },601)
                }else{
                    el.addClass('active');
                }



            });

   });
   $('.btn-close').on('click',function(){
             $('.picture-cont').removeClass('active')
   })



});
 </script>

<!-- popup -->
<div id="popup-full-mechanics">
    <div class="popup">
        <div class="popup-content for-mechanics">
            <div class="content clearfix">
                <a href="#" class="close">Close Popup</a>

                <div class="slider">
                    <div class="padding">
                    <h3>Some title goes here</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>



                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
                    </div><!-- .padding -->
                </div><!-- .slider -->
            </div>
        </div><!-- .popup-conten -->
    </div><!-- .popup -->
    <div class="blur"></div>
</div>
<!-- .popup -->

<!-- popup -->
<div id="popup-video">
    <div class="popup">
        <div class="popup-content for-upload">
            <a href="#" class="close">CLOSE</a>
            <iframe width="100%" height="100%" src="//www.youtube.com/embed/q-E36QOtHi0?rel=0" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
    <div class="blur"></div>
</div>
<!-- .popup -->
</body>
</html>
