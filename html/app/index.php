
<?php include 'header.php' ;?>
    <section class="main">
        <section id="home" class="light-bg clearfix">
            <div class="container">
                <div class="home-details">
                    <div class="how-to">
                        <div class="ribbon">HOW TO JOIN</div>
                        <img src="assets/img/anne-home.png">
                        <h1 class="hashtag">#ANNE<span>DERARMS</span></h1>
                        <p>
                            Like Anne, you can fight darkness with the Belo Beauty Duo!
                            Simply send us your best superhero pose on Instagram, and you can win a Belo Beauty Duo Kit PLUS a trip to Boracay!
                        </p>
                        <h2 class="komika">Join now!</h2>

                    </div>
                    <div class="tweet-feed">
                        <div class="ribbon">PHOTO ENTRIES</div>
                        <ul class="people-lists">
                            <?php for($i =0;$i < 36; $i++){?>
                            <li style="background-image:url(assets/img/sample-pic.png)" data-username="JGS" data-desc="#NWPower" data-large="assets/img/sample-pic-lg.png" data-fullname="assets/img/sample- pic-mg.png" >

                            </li>
                            <?php } ?>
                            <div class="picture-cont">
                                <div class="btn-close"></div>
                                <div class="picture-info">
                                            <ul class="people-details">
                                                <li> <span class="username"><i class="fa fa-instagram"></i> lorem ipsum</span></li>
                                                <li> <span class="fullname">Jane Doe</span></li>
                                            </ul>
                                            <div class="clearfix"></div>

                                            <span class="desc">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda eos illum tempora dignissimos aspernatur distinctio,
                                            </span>

                                           <div class="share-this clearfix">
                                                <ul>
                                                    <li> <a href="http://facebook.com" class="share-facebook">lorem</a></li>
                                                    <li><a href="http://twitter.com" class="share-twitter">lorem</a></li>
                                                </ul>


                                           </div>

                                </div>

                            </div>

                        </ul>
                        <div class="search">
                            <!--<label for="data[address][0]">Address 1</label>-->
                                <input placeholder="Type your name to search your entry" type="text" name="search" class="search-feed">
                                <button><i class="fa fa-search"></i></button>
                        </div>





                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>
        </section>

        <section id="mechanics" class="light-bg clearfix" >

            <div class="container">

                <div class="mechanics-details">
                    <div class="left">
                        <img src="assets/img/twitter-logo.png">
                        <h1 class="hashtag">#BELOBEAUTYDuO</h1>
                        <h1 class="hashtag">#ANNE<span>DERARMS</span></h1>

                        <p>Join us in our mission of banishing dark underarms and win some awesome prizes to boot!</p>

                        <div class="text-center">
                            <img src="assets/img/beauty-deo.png">
                        </div>
                    </div>

                    <div class="right">
                        <ul class="caveman-mechanics">
                            <li>
                                <i>1</i>
                                <div class="copy">
                                    <p>Take a photo of your SUPERHERO pose. </p>


                                </div>
                            </li>

                            <li>
                                <i>2</i>
                                <div class="copy">
                                    <p>
                                        Upload to Instagram with the hashtags
                                        <b>#BeloBeautyDuo</b> and <b>#ANNEderarms</b>.
                                    </p>


                                </div>
                            </li>

                            <li>
                                <i>3</i>
                                <div class="copy">
                                        <p>
                                            Tag <b>@belo_essentials and</b>
                                            you’re good to go!
                                        </p>

                                    <a href="#" class="btn"><i class="fa fa-arrow-right"></i> VIEW FULL MECHANICS</a>
                                </div>
                            </li>

                        </ul>
                         <div class="disclaimer">
                             <p>
                                *Set your Instagram accounts to public so we can see your entries!
                            </p>
                            <p>
                              *Contest runs from April 18 – May 17, 2015.
                             </p>
                         </div>
                    </div>

                    <div class="clearfix"></div>
                </div>


            </div>
        </section>

        <section id="prizes" class="dark-bg clearfix" >
            <div class="container">
                <div class="title">
                    <h4>
                            5 weekly winners get a special ANNEderarms Kit.
                     </h4>
                     <p>
                         The   <b>grand prize winner</b> will receive the following:
                     </p>
                </div>
                <div class="content">



                    <ul class="prizes clearfix">
                        <li>
                            <figure><img src="assets/img/prize-1.png" alt="3-day trip to Boracay for four"></figure>
                            4 underarm RevLite sessions
                            from the Belo Medical Group
                        </li>
                        <li>
                            <figure><img src="assets/img/prize-2.png" alt="up to four Underarm Hair Removal sessions by Belo Medical Group"></figure>
                            Beauty Duo Kit
                        </li>
                        <li>
                            <figure><img src="assets/img/prize-3.png" alt="one RevLite session by Belo Medical Group"></figure>
                            3 day/2 night trip for 4
                            to BORACAY!
                        </li>
                    </ul>


                </div>


            </div><!-- .container -->
        </section><!-- #prizes -->



        <section id="product" class="light-bg clearfix" >
            <div class="container">
                <h3>Say good bye to your underarm problems with Belo Essentials Beauty Deo!</h3>

                <ul class="skin">
                    <li><img src="assets/img/skin-sweaty.png"> <span>SWEATY</span></li>
                    <li><img src="assets/img/skin-rough.png"> <span>ROUGH</span></li>
                    <li><img src="assets/img/skin-chicken.png"> <span>CHICKEN SKIN</span></li>
                    <li><img src="assets/img/skin-dark.png"> <span>DARK</span></li>
                    <li><img src="assets/img/skin-red.png"> <span>RED</span></li>
                </ul>

                <div class="product-container">
                    <div class="bubble-box">
                      Fighting underarm darkness? Power-up with the Belo Beauty Duo! Use the Whitening Anti-Perspirant Deodorant by day for smoother skin and minimized pores. Then apply the Whitening Cream at night to whiten skin at the cellular level.
                      Deo by day and the Whitening Cream by night – banish dark underarms with the Belo Beauty Duo!
                    </div>
                    <div class="clearfix"></div>
                   <div class="products">
                             <div class="clearfix hashtag-img">
                                 <img src="assets/img/comics.png"/>
                             </div>
                            <div class=" per-product">
                                        <div class="product-img p1">

                                        </div>
                                        <div class="per-details">
                                                        <div class="details-copy">
                                                                    <span>
                                                                        Underarm<br/>
                                                                        Whitening<br/>
                                                                        Cream (40g)<br/>
                                                                        <b>SRP: 349.75</b><br/>
                                                                    </span><br/>
                                                                    <a href="#" class="btn btn-default btn-medium">BUY NOW</a>
                                                        </div>
                                        </div>
                            </div>
                              <div class=" per-product">
                                        <div class="product-img p2">

                                        </div>
                                        <div class="per-details">
                                                         <div class="details-copy">
                                                                    <span>
                                                                        Whitening<br/>
                                                                        Anti-Perspirant Deodorant<br/>
                                                                        Original Scent (40mL)<br/>
                                                                        <b>SRP: 92.75</b><br/>
                                                                    </span><br/>
                                                                    <a href="#" class="btn btn-default btn-medium">BUY NOW</a>
                                                        </div>
                                        </div>
                            </div>
                              <div class=" per-product">
                                        <div class="product-img p3">

                                        </div>
                                        <div class="per-details">
                                                             <div class="details-copy">
                                                                    <span>
                                                                        Whitening<br/>
                                                                        Anti-Perspirant Deodorant<br/>
                                                                        Shower Scent (40mL)<br/>
                                                                        <b>SRP: 92.75</b><br/>
                                                                    </span><br/>
                                                                    <a href="#" class="btn btn-default btn-medium">BUY NOW</a>
                                                        </div>
                                        </div>
                            </div>
                              <div class=" per-product">
                                        <div class="product-img p4">

                                        </div>
                                        <div class="per-details">
                                                         <div class="details-copy">
                                                                    <span>
                                                                        Whitening<br/>
                                                                        Anti-Perspirant Deodorant<br/>
                                                                        Original Scent (140mL)<br/>
                                                                        <b>SRP: 184.75</b><br/>
                                                                    </span><br/>
                                                                    <a href="#" class="btn btn-default btn-medium">BUY NOW</a>
                                                        </div>
                                        </div>
                            </div>
                              <div class=" per-product">
                                        <div class="product-img p5">

                                        </div>
                                        <div class="per-details">
                                                         <div class="details-copy">
                                                                    <span>
                                                                        Whitening<br/>
                                                                        Anti-Perspirant Deodorant<br/>
                                                                        Shower Scent (140mL)<br/>
                                                                        <b>SRP: 184.75</b><br/>
                                                                    </span><br/>
                                                                    <a href="#" class="btn btn-default btn-medium">BUY NOW</a>
                                                        </div>
                                        </div>
                            </div>

                    </div>
                </div>
            </div><!-- .container -->
        </section><!-- #product -->
    </section><!-- .main content -->

<?php include 'footer.php' ;?>
