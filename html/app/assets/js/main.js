(function($){
    $.fn.offsetRelative = function(top){
        var $this = $(this);
        var $parent = $this.offsetParent();
        var offset = $this.position();
        if(!top) return offset; // Didn't pass a 'top' element
        else if($parent.get(0).tagName == "BODY") return offset; // Reached top of document
        else if($(top,$parent).length) return offset; // Parent element contains the 'top' element we want the offset to be relative to
        else if($parent[0] == $(top)[0]) return offset; // Reached the 'top' element we want the offset to be relative to
        else { // Get parent's relative offset
            var parent_offset = $parent.offsetRelative(top);
            offset.top += parent_offset.top;
            offset.left += parent_offset.left;
            return offset;
        }
    };
    $.fn.positionRelative = function(top){
        return $(this).offsetRelative(top);
    };
}(jQuery));

$(function() {
    preventDefault()
    popup()
    navScroll();



    // FitVids
    $(".video_responsive").fitVids();


    $('#mininav').on('click', function() {
        $('.navigation').slideToggle(200)
        $('.navigation').toggleClass('active')
    })

    $('.navigation a').on('click', function() {
        if ($(window).width() <= 768) {
            $('.navigation').slideToggle(200)
            $('.navigation').toggleClass('active')
        }
    })


})

function preventDefault() {
    // Prevent Default
    $('a').on('click', function(event) {
        event.preventDefault()
    })
}

function popup() {
    //  Hide
    $('#popup-video, #popup-full-mechanics').hide()

    // Close
    $('.close').on('click', function() {
        $(this).parents('.popup').parent().hide()
    })

    $('.see-video').on('click', function() {
        $('#popup-video').show()
    })

    $('.show_full_mechanics').on('click', function() {
        $('#popup-full-mechanics').show()
    })
}

function navScroll() {

    var links = $('.navigation a[href^="#"]')
    var linksA = $('.navigation a[href^="#"]')
    var scrollables = $('html, body')

    var homeSection = $('#home').offset().top
    var prizesSection = $('#prizes').offset().top
    var mechanicsSection = $('#mechanics').offset().top
    var productSection = $('#product').offset().top

    function doScroll(e) {
        var link = $(this)
        var id = $(this).attr('href')
        var top = $(id).offset().top - ($('header.main').height() + 25);

        e.preventDefault()

        scrollables.animate({
            scrollTop: top
        })

    }

    $(window).scroll(function() {
        var winPos = $(window).scrollTop(),
            homeHeight = $('#home').height(),
            videoBtn = $('.see-video-again'),
            homeEnd = $('#home').offset().top + $('#home').height() - 200,
            prizesEnd = $('#prizes').offset().top + $('#prizes').height() - 200,
            mechanicsEnd = $('#mechanics').offset().top + $('#mechanics').height() - 200,
            productEnd = $('#product').offset().top + $('#product').height() - 200


        // Nav Active
        if (winPos < homeEnd) {
            $('.navigation a[href^="#"]').removeClass('active')
            $('.navigation a[href^="#home"]').addClass('active')


        } else if (winPos > prizesEnd && winPos < mechanicsEnd) {
            $('.navigation a[href^="#"]').removeClass('active')
            $('.navigation a[href^="#mechanics"]').addClass('active')
        } else if (winPos > mechanicsEnd && winPos < productEnd) {
            $('.navigation a[href^="#"]').removeClass('active')
            $('.navigation a[href^="#product"]').addClass('active')
        }

    })
    links.on('click', doScroll);






}
