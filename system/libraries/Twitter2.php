<?php

/**
 * 
 * @author anthony
 * 
 */
class TwitterResult
{
    private $_data;

    public function __construct($result)
    {
        if(is_string($result)){
            $result = json_decode($result, true);
        }

        $this->_data = $result;
    }

    public function __get($key)
    {
        if(!isset($this->_data[$key])){
            return null;
        }
        return $this->_data[$key];
    }

    public function next()
    {
        if(!empty($this->_data['next_page'])){
            $curl = curl_init($this->_data['next_page']);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $result = json_decode((string) curl_exec($curl), true);
            if(!empty($result)){
                return new self($result);
            }
        }
        return null;
    }

    public function previous()
    {
        if(!empty($this->_data['previous_page'])){
            $curl = curl_init($this->_data['previous_page']);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $result = json_decode((string) curl_exec($curl), true);
            if(!empty($result)){
                return new self($result);
            }
        }
        return null;
    }
}

/**
 * 
 * @author anthony
 * 
 */
class Twitter
{
    /**
     * 
     * @param string|array $query
     * @return array|null
     */
    public function search($query)
    {
        $curl = null;
        if(is_string($query)){
            $curl = curl_init($query);
        } else if(is_array($query)){
            $curl = curl_init('http://search.twitter.com/search.json?'.http_build_query($query));
        }
        //'&include_entities=true&result_type=recent'
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = json_decode((string) curl_exec($curl), true);
        if(empty($result)){
            return null;
        }
        return new TwitterResult($result);
    }
}