<?php

/**
 * 
 * @author anthony
 * 
 */
class Tweet
{
    /**
     * 
     * @var array
     */
    private $tweet = array();

    /**
     * 
     * @param array $tweet
     * @return Tweet
     */
    public function set_data(array $tweet)
    {
        $this->tweet = $tweet;
        return $this;
    }

    /**
     * 
     * @return array
     */
    public function get_data()
    {
        return $this->tweet;
    }

    /**
     * 
     * @param string $key
     * @return mixed
     */
    public function __get($key)
    {
        return isset($this->tweet[$key]) ? $this->tweet[$key] : null;
    }

    /**
     * 
     * @return array
     */
    public function hash_tags()
    {
        $tags = array();
        if (!empty($this->tweet['entities']['hashtags'])) {
            foreach ($this->tweet['entities']['hashtags'] as $tag) {
                $tags[] = $tag['text'];
            }
        }
        return $tags;
    }

    /**
     * 
     * @return array
     */
    public function mentions()
    {
        $tags = array();
        if (!empty($this->tweet['entities']['user_mentions'])) {
            foreach ($this->tweet['entities']['user_mentions'] as $tag) {
                $tags[] = $tag['screen_name'];
            }
        }
        return $tags;
    }

    /**
     * 
     * @param string $mention
     * @return boolean
     */
    public function has_mention($mention)
    {
        $mention = strtolower(ltrim($mention, '@'));
        if (!empty($this->tweet['entities']['user_mentions'])) {
            foreach ($this->tweet['entities']['user_mentions'] as $tag) {
                if ($mention === strtolower($tag['screen_name'])) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 
     * @param string $hash
     * @return boolean
     */
    public function has_hash_tag($hash)
    {
        $hash = strtolower(ltrim($hash, '#'));
        if (!empty($this->tweet['entities']['hashtags'])) {
            foreach ($this->tweet['entities']['hashtags'] as $tag) {
                if ($hash === strtolower($tag['text'])) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 
     * @return string
     */
    public function text()
    {
        return isset($this->tweet['text']) ? $this->tweet['text'] : null;
    }

    /**
     * 
     * @param string $format
     * @return string
     */
    public function date_created($format = 'Y-m-d H:i:s')
    {
        if (!isset($this->tweet['created_at'])) {
            return null;
        }

        return date($format, strtotime($this->tweet['created_at']));
    }

    /**
     * 
     * @param string $key
     * @return mixed
     */
    public function user($key = null)
    {
        if (!$key) {
            return $this->tweet['user'];
        }
        return isset($this->tweet['user'][$key]) ? utf8_decode($this->tweet['user'][$key]) : null;
    }
}