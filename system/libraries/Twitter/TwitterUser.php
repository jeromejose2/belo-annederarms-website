<?php

/**
 * 
 * @author anthony
 * 
 */
class TwitterUser
{
    /**
     * 
     * @param array $data
     */
    private $data = array();

    /**
     * 
     * @param array $data
     * @return TwitterUser
     */
    public function set_data(array $data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * 
     * @param string $key
     * @return string|array
     */
    public function __get($key)
    {
        return isset($this->data[$key]) ? $this->data[$key] : null;
    }

    /**
     * 
     * @param string $format
     * @return string
     */
    public function date_created($format = 'Y-m-d H:i:s')
    {
        if (!isset($this->data['created_at'])) {
            return null;
        }

        return date($format, strtotime($this->data['created_at']));
    }
}