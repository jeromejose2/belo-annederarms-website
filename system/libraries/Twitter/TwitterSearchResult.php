<?php

require_once 'TwitterResult.php';

/**
 * 
 * @author anthony
 * 
 */
class TwitterSearchResult extends TwitterResult
{
    /**
     * 
     * 
     * @var array
     */
    private $metaTwitSearch;

    /**
     * 
     * @param array $data
     * @return TwitterResult
     */
    public function set_metadata(array $data)
    {
        $this->metaTwitSearch = $data;
        return $this;
    }

    /**
     * 
     * @var Twitter
     */
    public function get_adapter()
    {
        return $this->twitter;
    }

    /**
     * 
     * @return array
     */
    public function get_metadata($key = null)
    {
        if ($key) {
            return isset($this->metaTwitSearch[$key]) ? $this->metaTwitSearch[$key] : null;
        }
        return $this->metaTwitSearch;
    }

    /**
     * 
     * @return string
     */
    public function next_url()
    {
        return $this->get_metadata('next_results');
    }

    /**
     * 
     * @return array
     */
    public function next_params()
    {
        $params = array();
        $url = $this->get_metadata('next_results');
        if ($url) {
            parse_str(ltrim($url, '?'), $params);
        }
        return $params;
    }

    /**
     * 
     * @return string
     */
    public function refresh_url()
    {
        return $this->get_metadata('refresh_url');
    }

    /**
     * 
     * @return array
     */
    public function refresh_params()
    {
        $params = array();
        $url = $this->get_metadata('refresh_url');
        if ($url) {
            parse_str(ltrim($url, '?'), $params);
        }
        return $params;
    }

    /**
     * 
     * @return TwitterResult
     */
    public function next()
    {
        $query = urldecode($this->get_metadata('query'));
        if (!$query) {
            return null;
        }

        $options = array();
        $next = $this->get_metadata('next_results');
        if ($next) {
            parse_str(ltrim($next, '?'), $options);
        } else {
            // $options = array(
            //     'max_id' => (int) $this->get_metadata('max_id'),
            //     'count' => (int) $this->get_metadata('count'),
            //     'include_entities' => $this->get_metadata('include_entities'),
            //     'result_type' => $this->get_metadata('result_type')
            // );
            // $options['max_id'] -= ($options['count'] - 1);
            return null;
        }

        $options['media_type'] = $this->get_metadata('media_type');
        $result = $this->twitter->search($query, $options);
        if (!$result) {
            return null;
        }

        $this->exchangeArray($result->getArrayCopy());
        $this->set_metadata($result->get_metadata());
        return $this;
    }

    /**
     * 
     * @return boolean
     */
    public function has_next()
    {
        if ($this->get_metadata('next_results')) {
            return true;
        }
        return false;
    }
}