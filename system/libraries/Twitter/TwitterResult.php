<?php

/**
 * 
 * @author anthony
 * 
 */
class TwitterResult extends ArrayObject
{
    /**
     * 
     * @var Twitter
     */
    protected $twitter;

    /**
     * 
     * @param array $content
     */
    public function __construct(array $content, Twitter $adapter)
    {
        parent::__construct($content);
        $this->twitter = $adapter;
    }
}