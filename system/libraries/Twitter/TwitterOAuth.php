<?php

/**
 * 
 * @author anthony
 * 
 */
class TwitterOAuth
{
    /**
     * 
     * @var string
     */
    const VERSION = '1.1';

    /**
     * 
     * @var string
     */
    const SIGNATURE_METHOD = "HMAC-SHA1";

    /**
     * 
     * @var string
     */
    const REQUEST_URL = "https://api.twitter.com/oauth/request_token";

    /**
     * 
     * @var string
     */
    const AUTHORIZE_URL = "https://api.twitter.com/oauth/authorize";

    /**
     * 
     * @var string
     */
    const ACCESS_TOKEN_URL = "https://api.twitter.com/oauth/access_token";

    /**
     * 
     * @param string|array $input
     * @return string
     */
    public static function encode($input)
    {
        if (is_array($input)) {
            return array_map('TwitterOAuth::encode', $input);
        } elseif (is_scalar($input)) {
            return str_replace('+',' ',str_replace('%7E', '~', rawurlencode($input)));
        }
        return '';
    }

    /**
     * 
     * @param string $token
     * @return string
     */
    public static function get_authorize_url($token)
    {
        return self::AUTHORIZE_URL.'?oauth_token='.$token;
    }

    /**
     * 
     * @param string $url
     * @param array $params
     * @param string  $key
     * @param string $secret
     * @return string
     */
    public static function generate_url($url, array $params, $key, $secret)
    {
        if (!$params) {
            $params = self::get_oauth_params($key);
        } else {
            $params = array_merge(self::get_oauth_params($key), $params);
        }
        
        $keys = self::encode(array_keys($params));
        $values = self::encode(array_values($params));
        $params = array_combine($keys, $values);
        uksort($params, 'strcmp');

        // convert params to string 
        foreach ($params as $k => $v) {$pairs[] = self::encode($k).'='.self::encode($v);}
        $concatenatedParams = implode('&', $pairs);

        // form base string (first key)
        $baseString= "GET&".self::encode($url)."&".self::encode($concatenatedParams);
        // form secret (second key)
        $secret = self::encode($secret)."&";
        // make signature and append to params
        $params['oauth_signature'] = self::encode(base64_encode(hash_hmac('sha1', $baseString, $secret, TRUE)));

     // BUILD URL
        // Resort
        uksort($params, 'strcmp');
        // convert params to string 
        foreach ($params as $k => $v) {$urlPairs[] = $k."=".$v;}
        $concatenatedUrlParams = implode('&', $urlPairs);
        // form url
        $url = $url."?".$concatenatedUrlParams;
        return $url;
    }

    /**
     * 
     * @param string $key
     * @param string $secret 
     * @return string
     */
    public static function get_request_url($key, $secret)
    {
        return self::generate_url(self::REQUEST_URL, array(), $key, $secret);
    }

    /**
     * 
     * @param string $key
     * @return array
     */
    public static function get_oauth_params($key)
    {
        // Default params
        $params = array(
            "oauth_version" => self::VERSION,
            "oauth_nonce" => time(),
            "oauth_timestamp" => time(),
            "oauth_consumer_key" => $key,
            "oauth_signature_method" => self::SIGNATURE_METHOD
        );

        return $params;
    }
}