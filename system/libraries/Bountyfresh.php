<?php

/**
 * 
 * @author anthony
 * 
 */
class Bountyfresh
{
    /**
     * 
     * @var string
     */
    const API_URL = 'http://www.chicmomsclub.com/api/cmc/d48170846c1e937640ae7e9f6743089c3c2a9074/';

    /**
     * 
     * @var string
     */
    const DB_USER = 'nwshare_bounty';

    /**
     * 
     * @var string
     */
    const DB_PASS = 'P$Th~F0mBsTf';

    /**
     * 
     * @var string
     */
    const DB_NAME = 'nwshare_bountychooks';

    /**
     * 
     * @var string
     */
    const TABLE_FB_DETAILS = 'tbl_registrants_bf';

    /**
     * 
     * @var string
     */
    const TABLE_REGISTRANTS = 'tbl_app_registrants';

    /**
     * 
     * @var string
     */
    const TABLE_APPS = 'tbl_apps';

    /**
     * 
     * @var string
     */
    const DB_HOST = 'localhost';

    /**
     * 
     * @var resource
     */
    private $_curl;

    /**
     * 
     * @var string
     */
    private $_app_name;

    /**
     * 
     * @var int
     */
    private $_app_id;

    /**
     * 
     * @var PDO
     */
    private $_db;

    /**
     * 
     * @param int $app_name
     */
    public function __construct($app_id)
    {
        if(!session_id()){
            session_start();
        }
        $this->_curl = curl_init();
        $this->_db = new PDO(
            'mysql:host='.self::DB_HOST.';dbname='.self::DB_NAME,
            self::DB_USER,
            self::DB_PASS,
            array(
                PDO::ATTR_PERSISTENT => true
            )
        );
        $this->_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $app_id = end($app_id);
        if(!isset($_SESSION['appbountychooks_'.$app_id])){
            $_SESSION['appbountychooks_'.$app_id] = array();
        }
        $this->_app_id = (int) $app_id;
        $this->_app_name = 'appbountychooks_'.$app_id;
    }

    /**
     * Login method
     * Password must be encrypted first before passing as argument
     * to this method
     * @param string $username
     * @param string $password
     * @return boolean|array
     */
    public function login($email, $password = null)
    {
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            return false;
        }
        if($password !== null){
            $login = json_encode(array(
                'user' => $email,
                'pass' => $password
            ));
            curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($this->_curl, CURLOPT_URL, self::API_URL.'auth/'.$login);
            // debug(self::API_URL.'auth/'.$login);
            $response = json_decode(curl_exec($this->_curl), true);
            if(curl_error($this->_curl)){
                return curl_error($this->_curl);
            }
            if(!$response){
                return false;
            }
            // debug($response);
        }
        
        $stmt = $this->_db->prepare("SELECT * FROM ".self::TABLE_FB_DETAILS." WHERE email = :email AND password = :password");
        $stmt->bindValue(":email", $email, PDO::PARAM_STR);
        $stmt->bindValue(":password", $password, PDO::PARAM_STR);
        $stmt->execute();
        if($user = $stmt->fetch(PDO::FETCH_ASSOC)){
            $this->_set_session($user);
            return $user;
        } else {
            $this->_set_session($response);
            return $response;
        }
    }

    /**
     * 
     * @param string $fbid
     * @return int
     */
    public function delete($fbid)
    {
        $this->remove_to_app($fbid);
        $stmt = $this->_db->prepare("DELETE FROM ".self::TABLE_FB_DETAILS." WHERE fbid = :fbid");
        $stmt->bindValue(":fbid", $fbid);
        try{
            $stmt->execute();
        } catch(PDOException $ex){
            show_error($ex->getMessage());
        }
        return $stmt->rowCount();
    }

    /**
     * 
     * @param string $id
     * @return array|false
     */
    public function login_fbid($id)
    {
        $stmt = $this->_db->prepare("SELECT * FROM ".self::TABLE_FB_DETAILS." WHERE fbid = :fbid");
        $stmt->bindValue(":fbid", $id, PDO::PARAM_STR);
        $stmt->execute();
        if($user = $stmt->fetch(PDO::FETCH_ASSOC)){
            $this->_set_session($user);
            return $user;
        }
        return false;
    }

    /**
     * 
     * @param string $id
     * @return boolean
     */
    public function valid_fbid($id)
    {
        $stmt = null;
        $stmt = $this->_db->prepare("SELECT * FROM ".self::TABLE_REGISTRANTS." WHERE fbid = :fbid and app_id = {$this->_app_id}");
        $stmt->bindValue(":fbid", $id, PDO::PARAM_STR);
        $stmt->execute();
        if($user = $stmt->fetch(PDO::FETCH_ASSOC)){
            return true;
        }
        return false;
    }

    /**
     * 
     * @param array $data
     * @return void
     */
    private function _set_session($data)
    {
        $_SESSION[$this->_app_name] = $data;
    }

    /**
     * 
     * @param string $email
     * @return boolean
     */
    public function is_logged_in($username = null)
    {
        return !empty($_SESSION[$this->_app_name]);
    }

    /**
     * 
     * @param array $data
     * @param boolean $withApi
     * @param string $error_message
     * @return boolean
     */
    private function _register($data, $withApi, &$error_message = null)
    {
        $fbid = null;
        if(isset($data['fbid'])){
            $fbid = $data['fbid'];
            unset($data['fbid']);
        }
        if(empty($data['lname'])){
            $error_message = "Last name is empty";
            return false;
        } else if(empty($data['fname'])){
            $error_message = "First name is empty";
            return false;
        } else if(empty($data['email']) || !filter_var($data['email'], FILTER_VALIDATE_EMAIL)){
            $error_message = "Invalid email";
            return false;
        } else if($withApi && empty($data['mobile'])){
            $error_message = "Invalid mobile number";
            return false;
        } else if($withApi && empty($data['password'])){
            $error_message = "Password is empty";
            return false;
        } else if(empty($data['gender'])){
            $error_message = "Gender is empty";
            return false;
        } else if(empty($data['bday'])){
            $error_message = "Bday is empty";
            return false;
        } else if($withApi && empty($data['province'])){
            $error_message = "Province is empty";
            return false;
        } else if(empty($data['country'])){
            $error_message = "Country is empty";
            return false;
        } else if(empty($fbid)){
            $error_message = "FBID is required";
            return false;
        }
        $password = $data['password'];
        unset($data['password']);
        $data['pass'] = $password;
        $ret_url = self::API_URL.'add_mem/'.urlencode(json_encode($data));
        // debug($ret_url);
        $response = array();
        if($withApi){
            curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($this->_curl, CURLOPT_URL, $ret_url);
            $response = json_decode(curl_exec($this->_curl), true);
            if(curl_error($this->_curl)){
                $error_message = curl_error($this->_curl);
                return false;
            }
            if(!empty($response['error'])){
                $error_message = $response['remarks'];
                return false;
            }
        }
        // debug($response, true);
        $data['fbid'] = $fbid;
        $stmt = $this->_db->prepare("SELECT * FROM ".self::TABLE_REGISTRANTS." WHERE fbid = :fbid AND app_id = :app_id");
        $stmt->bindValue(":fbid", $data['fbid']);
        $stmt->bindValue(":app_id", $this->_app_id);
        try {
            $stmt->execute();
        } catch(PDOException $ex){
            show_error($ex->getMessage());
        }
        $error = $stmt->errorInfo();
        if(!empty($error[1]) || !empty($error[2])){
            show_error($error[1].' : '.$error[2]);
        }
        unset($data['pass']);
        $data['password'] = $password;
        foreach($data as &$val){
            $val = $this->_db->quote($val);
        }
        $hasExisting = $stmt->rowCount();
        $existingData = $stmt->fetch(PDO::FETCH_ASSOC);
        $insertId = isset($existingData['app_registrant_id']) ? $existingData['app_registrant_id'] : null;
        if($hasExisting !== 1){
            $stmt = null;
            // $data['date_registered'] = date('Y-m-d H:i:s');
            $stmt = $this->_db->prepare("INSERT INTO ".self::TABLE_FB_DETAILS."(".implode(',', array_keys($data)).") VALUES (".implode(',', $data).")");
            try {
                $stmt->execute();
            } catch(PDOException $ex){
                $error_message = $ex->getMessage();
                return false;
            }
            $error = $stmt->errorInfo();
            if(!empty($error[1]) || !empty($error[2])){
                $error_message = $error[1].' : '.$error[2];
                return false;
            } else if($stmt->rowCount() !== 1){
                return false;
            }

            $stmt = null;
            $stmt = $this->_db->prepare("INSERT INTO ".self::TABLE_REGISTRANTS."(fbid, app_id) VALUES(".$data['fbid'].",".$this->_app_id.")");
            try {
                $stmt->execute();
            } catch(PDOException $ex){
                show_error($ex->getMessage());
            }
            $error = $stmt->errorInfo();
            if(!empty($error[1]) || !empty($error[2])){
                show_error($error[1].' : '.$error[2]);
            } else if($stmt->rowCount() !== 1){
                show_error("Failed to insert");
            }
            $insertId = $this->_db->lastInsertId();
        }
        
        return $insertId;
    }

    /**
     * 
     * @param array $data
     * @param string $error_message
     * @return boolean
     */
    public function register($data, &$error)
    {
        return $this->_register($data, true, &$error);
    }

    /**
     * 
     * @param array $data
     * @param string $error_message
     * @return boolean
     */
    public function register_fb_details($data, &$error)
    {
        return $this->_register($data, false, &$error);
    }

    /**
     * 
     * @param string $fbid
     * @return int|false
     */
    public function register_to_app($fbid)
    {
        $stmt = $this->_db->prepare("SELECT * FROM ".self::TABLE_REGISTRANTS." WHERE fbid = :fbid AND app_id = {$this->_app_id}");
        $stmt->bindValue(":fbid", $fbid);
        try {
            $stmt->execute();
        } catch(PDOException $ex){
            show_error($ex->getMessage());
        }
        $error = $stmt->errorInfo();
        if(!empty($error[1]) || !empty($error[2])){
            show_error($error[1].' : '.$error[2]);
        } else if(!$stmt->rowCount()){
            $stmt = null;
            $stmt = $this->_db->prepare("INSERT INTO ".self::TABLE_REGISTRANTS."(fbid, app_id) VALUES({$fbid},{$this->_app_id})");
            try {
                $stmt->execute();
            } catch(PDOException $ex){
                show_error($ex->getMessage());
            }
            $error = $stmt->errorInfo();
            if(!empty($error[1]) || !empty($error[2])){
                show_error($error[1].' : '.$error[2]);
            }

            return $this->_db->lastInsertId();
        }

        return false;
    }

    /**
     * 
     * @return void
     */
    public function logout()
    {
        $_SESSION[$this->_app_name] = array();
    }

    /**
     * 
     * @return array|null
     */
    public function get_user()
    {
        return !empty($_SESSION[$this->_app_name]) ? $_SESSION[$this->_app_name] : null;
    }

    /**
     * 0 - invalid
     * 1 - valid
     * @param string $email
     * @return int
     */
    public function valid_email($email, &$from_chic_moms = false)
    {
        $json = json_encode(array(
            'email' => $email
        ));
        curl_setopt($this->_curl, CURLOPT_URL, self::API_URL.'valid_email/'.urlencode($json));
        curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, true);
        $validFromChics = (int) curl_exec($this->_curl);

        $stmt = null;
        $stmt = $this->_db->prepare("SELECT COUNT(*) AS `total` FROM ".self::TABLE_FB_DETAILS." WHERE email = :email");
        $stmt->bindValue(":email", $email, PDO::PARAM_STR);
        $valid = null;
        try {
            $stmt->execute();
            if($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                $valid = (int) $row['total'];
            }
        } catch(PDOException $ex) {
            return false;
        }
        $error = $stmt->errorInfo();
        if(!empty($error[1]) || !empty($error[2])){
            $error_message = $error[1].' : '.$error[2];
            return false;
        }
        if(!$valid && $validFromChics){
            // MEron sa API at wala sa atin
            $from_chic_moms = true;
        } else if($valid && $validFromChics){
            // Meron sa API at meron sa atin
            $from_chic_moms = false;
        }
        return $valid || $validFromChics;
    }

    /**
     * 
     * @param string $fbid
     * @return int
     */
    public function remove_to_app($fbid)
    {
        $stmt = $this->_db->prepare("DELETE FROM ".self::TABLE_REGISTRANTS." WHERE fbid = :fbid AND app_id = {$this->_app_id}");
        $stmt->bindValue(":fbid", $fbid);
        try{
            $stmt->execute();
        } catch(PDOException $ex){
            show_error($ex->getMessage());
        }
        return $stmt->rowCount();
    }

    /**
     * 0 - invalid
     * 1 - valid
     * @param string $mobile
     * @return int
     */
    public function is_registered_mobile($mobile)
    {
        $json = json_encode(array(
            'mobile' => $mobile
        ));
        curl_setopt($this->_curl, CURLOPT_URL, self::API_URL.'valid_mobile/'.urlencode($json));
        curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, true);
        return (int) curl_exec($this->_curl);
    }

    /**
     * 
     * @param string $fbid
     * @return boolean
     */
    public function save_user_details($fbid, $email)
    {
        $return = true;
        $user_data = $this->get_user_data($email);
        $is_registered = $this->valid_fbid($fbid);
        $is_existing_member = $this->valid_email($email,$chick_moms);
        if($chick_moms){
            $post = array(
                'fbid' => $fbid,
                'fname'    =>$user_data['user_fname'],
                'lname'    =>$user_data['user_lname'],
                'gender'   =>$user_data['user_gender'],
                'bday'     =>$user_data['user_bday'],
                'province' =>$user_data['user_province'],
                'email'    =>$user_data['user_email'],
                'password' =>'nuworks',
                'mobile'   =>$user_data['user_mobile'],
                'country'  =>$user_data['user_country']
            );
            $this->register_fb_details($post, $error);
        } else if($is_existing_member){
            if(!$is_registered){
                $this->register_to_app($fbid);
            }
        } else {
            $return = false;
        }
        return $return;
    }

    /**
     * 
     * param:
     * array(
     *  'where' => array(
     *      'field' => 'condition'
     *   ),
     *  'order' => array(
     *      'field' => 'DESC'
     *    )
     * )
     * return:
     * array(
     *  'rows' => array(),
     *  'total' => 100
     * 
     * )
     * @param array $condition
     * @param int $limit
     * @param int $page
     * @return array
     * $this->bountyfresh->get_users()
     */
    public function get_users($condition = array(), $limit = 30, $page = 1)
    {
        $compiled = '';
        if(!empty($condition['where']) && is_array($condition['where'])){
            $where = array();
            $compiled .= ' AND ';
            foreach($condition['where'] as $field => $cond){
                if (is_array($cond)) {
                    $where[] = "`d`.`{$field}` ".implode("AND `d`.`{$field}` ", $cond);
                } else {
                    $where[] = "`d`.`{$field}` {$cond}";
                }
                
            }
            $compiled .= implode(' AND ', $where);
        }
        if(!empty($condition['order'])){
            $compiled .= ' ORDER BY ';
            if(is_string($condition['order'])){
                $compiled .= $condition['order'];
            } else if(is_array($condition['order'])){
                $orders = array();
                foreach($condition['order'] as $order){
                    $order = explode(' ', preg_replace('/\s+/', ' ', $order));
                    $orders[] = '`d`.`'.$order[0].'` '.@$order[1];
                }
                $compiled .= implode(',', $orders);
            }
        }
        $results = array('rows' => null, 'total' => 0);
        try {
            if($condition){
                $condition = ' AND '.$condition;
            }
            $stmt = null;
            try {
                $stmt = $this->_db->prepare("SELECT COUNT(*) AS `total` FROM ".self::TABLE_FB_DETAILS." d JOIN ".self::TABLE_REGISTRANTS." r ON d.fbid = r.fbid WHERE r.app_id = {$this->_app_id} {$compiled}");
                // print_r("SELECT COUNT(*) AS `total` FROM ".self::TABLE_FB_DETAILS." d JOIN ".self::TABLE_REGISTRANTS." r ON d.fbid = r.fbid WHERE r.app_id = {$this->_app_id} {$compiled}");
                $stmt->execute();
            } catch(PDOException $ex){
                show_error($ex->getMessage());
            }
            
            $error = $stmt->errorInfo();
            if(!empty($error[1]) || !empty($error[2])){
                show_error($error[2]);
            }
            $results['total'] = $stmt->fetch(PDO::FETCH_ASSOC);
            $results['total'] = (int) $results['total']['total'];
            $page = $limit * (int) ($page - 1);
            $stmt = null;
            if(!$limit){
                $stmt = $this->_db->prepare("SELECT * FROM ".self::TABLE_FB_DETAILS." d JOIN ".self::TABLE_REGISTRANTS." r ON d.fbid = r.fbid  WHERE r.app_id = {$this->_app_id} {$compiled}");
            } else {
                $stmt = $this->_db->prepare("SELECT * FROM ".self::TABLE_FB_DETAILS." d JOIN ".self::TABLE_REGISTRANTS." r ON d.fbid = r.fbid  WHERE r.app_id = {$this->_app_id} {$compiled} LIMIT {$limit} OFFSET {$page}");
                // echo "SELECT * FROM ".self::TABLE_FB_DETAILS." d JOIN ".self::TABLE_REGISTRANTS." r ON d.fbid = r.fbid  WHERE r.app_id = {$this->_app_id} {$compiled} LIMIT {$limit} OFFSET {$page}";
            }
            $stmt->execute();
            $error = $stmt->errorInfo();
            if(!empty($error[1]) || !empty($error[2])){
                show_error($error[2]);
            }
            $results['rows'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch(PDOException $ex) {
            show_error($ex->getMessage());
        }
        return $results;
    }

    /**
     * array(
     *  'rows' => array(),
     *  'total' => 100
     * 
     * )
     * @param string $condition
     * @param int $limit
     * @param int $page
     * @return array
     * $this->bountyfresh->get_users()
     */
    public function get_users_old($condition = '', $limit = 30, $page = 1)
    {
        $results = array('rows' => null, 'total' => 0);
        try {
            $stmt = null;
            if($condition){
                $condition = ' AND '.$condition;
            }
            $stmt = $this->_db->prepare("SELECT COUNT(*) AS `total` FROM ".self::TABLE_FB_DETAILS." d WHERE 1 {$condition}");
            $stmt->execute();
            $error = $stmt->errorInfo();
            if(!empty($error[1]) || !empty($error[2])){
                show_error($error[2]);
            }
            $results['total'] = $stmt->fetch(PDO::FETCH_ASSOC);
            $results['total'] = (int) $results['total']['total'];
            $page = $limit * (int) ($page - 1);
            $stmt = null;
            if(!$limit){
                $stmt = $this->_db->prepare("SELECT * FROM ".self::TABLE_FB_DETAILS." d WHERE 1 {$condition}");
            } else {
                $stmt = $this->_db->prepare("SELECT * FROM ".self::TABLE_FB_DETAILS." d WHERE 1 {$condition} ORDER BY d.date_registered DESC LIMIT {$limit} OFFSET {$page}");
            }
            $stmt->execute();
            $error = $stmt->errorInfo();
            if(!empty($error[1]) || !empty($error[2])){
                show_error($error[2]);
            }
            $results['rows'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch(PDOException $ex) {
            show_error($ex->getMessage());
        }
        return $results;
    }

    /**
     * 
     * @param string $fbid
     * @return array
     */
    public function get_user_by_fbid($fbid)
    {
        $stmt = null;
        $stmt = $this->_db->prepare("SELECT * FROM ".self::TABLE_FB_DETAILS." d JOIN ".self::TABLE_REGISTRANTS." r ON d.fbid = r.fbid  WHERE r.app_id = {$this->_app_id} AND r.fbid = :fbid LIMIT 1");
        $stmt->bindValue(":fbid", $fbid);
        try {
            $stmt->execute();
        } catch(PDOException $ex){
            show_error($ex->getMessage());
        }
        $error = $stmt->errorInfo();
        if(!empty($error[1]) || !empty($error[2])){
            show_error($error[2]);
        }
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * 
     * @param string $email
     * @return array
     */
    public function get_user_data($email)
    {
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            return false;
        }
        $request = urlencode(json_encode(array('email' => $email)));
        curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->_curl, CURLOPT_URL, self::API_URL.'get_info/'.$request);
        // var_dump(self::API_URL.'get_info/'.$request);
        // var_dump(self::API_URL.'get_info/'.$request);
        if(curl_errno($this->_curl)){
            show_error(curl_error($this->_curl));
        }
        $result = (string) curl_exec($this->_curl);
        $return = json_decode($result, true);
        return $return;
        // return $result;
    }
}

?>