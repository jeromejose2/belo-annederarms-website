<?php

require_once 'Twitter/TwitterSearchResult.php';
require_once 'Twitter/TwitterFollowers.php';
require_once 'Twitter/TwitterUsers.php';
require_once 'Twitter/TwitterUser.php';
require_once 'Twitter/TwitterOAuth.php';
require_once 'Twitter/Tweet.php';

/**
 * 
 * @author anthony
 * 
 */
class Twitter
{
    /**
     * 
     * @var string
     */
    const USER_AGENT = 'NuWorks Twitter SDK v1.1';

    /**
     * 
     * @var string
     */
    const REST_API_HOST = 'https://api.twitter.com/';

    /**
     * 
     * @var string
     */
    private $key;

    /**
     * 
     * @var string
     */
    private $secret;

    /**
     * 
     * @var string
     */
    private $appToken = null;

    /**
     * 
     * @var array
     */
    private $oauthToken = array();

    /**
     * 
     * @var string
     */
    private $sessionAppkey;

    /**
     * 
     * @var string
     */
    private $sessionAccessTokenKey;

    /**
     * 
     * @var string
     */
    private $sessionAccessTokenSecret;

    /**
     * 
     * @var string
     */
    private $sessionUserInfoKey;

    /**
     * 
     * @var string
     */
    private $sessionUserkey;

    /**
     * 
     * @var string|callable
     */
    private $callback = null;

    /**
     * 
     * @param string $key
     * @param string $secret
     */
    public function __construct($config = array())
    {
        @session_start();
        if ($config) {
            $this->set_config($config['key'], $config['secret']);
            if (isset($config['callback'])) {
                $this->set_callback($config['callback']);
            }
            return;
        } elseif (defined('CI_VERSION')) {
            $CI = & get_instance();
            $key = $CI->config->item('twitter_consumer_key');
            $secret = $CI->config->item('twitter_consumer_secret');
            $callback = $CI->config->item('twitter_callback');
            if ($callback) {
                $this->set_callback($callback);
            }
            if ($key && $secret) {
                $this->set_config($key, $secret);
                return;
            }
        }
        die("Twitter Consumer Key and Secret Must be Set");
    }

    /**
     * 
     * @param string|callable $callback
     * @return Twitter
     */
    public function set_callback($callback)
    {
        $this->callback = $callback;
        return $this;
    }

    /**
     * 
     * @access protected
     * @return string
     */
    protected function get_callback()
    {
        return TwitterOAuth::encode((string) (is_callable($this->callback) ? $this->callback() : $this->callback));
    }

    /**
     * 
     * @param string $key
     * @param string $secret
     * @return Twitter
     */
    public function set_config($key, $secret)
    {
        $this->key = TwitterOAuth::encode($key);
        $this->secret = TwitterOAuth::encode($secret);
        $this->sessionAppkey = 'twitter_app_auth_'.$this->key;
        $this->sessionUserkey = 'twitter_user_auth_'.$this->key;
        $this->sessionAccessTokenSecret = 'twitter_access_token_secret_'.$this->key;
        $this->sessionAccessTokenKey = 'twitter_access_token_'.$this->key;
        $this->sessionUserInfoKey = 'twitter_user_info_'.$this->key;
        return $this;
    }

    /**
     * 
     * @return string
     */
    public function get_app_token()
    {
        if (isset($_SESSION[$this->sessionAppkey])) {
            if (!$this->appToken) {
                $this->appToken = $_SESSION[$this->sessionAppkey];
            }
            return $this->appToken;
        } else {
            $this->appToken = null;
        }

        $bearer_token = $this->key.':'.$this->secret;
        $base64_encoded_bearer_token = base64_encode($bearer_token);

        $url = self::REST_API_HOST."oauth2/token"; // url to send data to for authentication
        $headers = array( 
            "POST /oauth2/token HTTP/1.1", 
            "Host: api.twitter.com", 
            "User-Agent: ".self::USER_AGENT,
            "Authorization: Basic ".$base64_encoded_bearer_token."",
            "Content-Type: application/x-www-form-urlencoded;charset=UTF-8", 
            "Content-Length: 29"
        ); 

        $retrievedhtml = $this->curl($url, array('grant_type' => 'client_credentials'), $headers, 'POST', false, true);
        $output = explode("\n", $retrievedhtml);
        $bearer_token = '';
        foreach ($output as $line) {
            if ($line === false) {
                // there was no bearer token
            } else {
                $bearer_token = $line;
            }
        }
        $bearer_token = json_decode($bearer_token, true);
        if (isset($bearer_token['error']) && $bearer_token['error']['code']) {
            die($bearer_token['error']['code'].' : '.$bearer_token['error']['message']);
        }
        if (isset($bearer_token['access_token'])) {
            $this->appToken = $bearer_token['access_token'];
            $_SESSION[$this->sessionAppkey] = $this->appToken;
        }
        return $this->appToken;
    }

    /**
     * Invalidates the Bearer Token
     * Should the bearer token become compromised or need to be invalidated for any reason,
     * call this method/function.
     * @param string $bearer_token
     * @return string
     */
    public function invalidate_token($bearer_token)
    {
        $consumer_token = $this->key.':'.$this->secret;
        $base64_encoded_consumer_token = base64_encode($consumer_token);

        $url = self::REST_API_HOST."oauth2/invalidate_token"; // url to send data to for authentication
        $headers = array( 
            "POST /oauth2/invalidate_token HTTP/1.1", 
            "Host: api.twitter.com", 
            "User-Agent: ".self::USER_AGENT, 
            "Authorization: Basic ".$base64_encoded_consumer_token."", 
            "Accept: */*", 
            "Content-Type: application/x-www-form-urlencoded", 
            "Content-Length: ".(strlen($bearer_token) + 13).""
        );
        
        $retrievedhtml = $this->curl($url, array('access_token' => $bearer_token), $headers, 'POST', false, true);
        return $retrievedhtml;
    }

    /**
     * 
     * Search
     * Basic Search of the Search API
     * Based on https://dev.twitter.com/docs/api/1.1/get/search/tweets
     * Base the search operators in https://dev.twitter.com/docs/using-search
     * @param string $query
     * @param string|array $options
     * @return TwitterSearchResult
     */
    public function search($query, $options = array())
    {
        $bearer_token = $this->get_app_token();
        // var_dump($bearer_token);
        $url = null;
        $default = array(
            'result_type' => 'mixed',
            'count' => 15,
            'include_entities' => true
        );
        if ($query) {
            if (is_string($query)) {
                $default['q'] = trim($query);
            } elseif (is_array($query)) {
                $default['q'] = implode(' OR ', array_map('trim', $query));
            }
        }

        if (empty($default['q'])) {
            die('Query Parameter is missing');
        }

        $media_type = null;
        if ($options) {
            if (is_array($options)) {
                foreach ($options as $key => $value) {
                    $default[$key] = $value;
                }
            } elseif (is_string($options)) {
                parse_str(ltrim($options, '?'), $tmp);
                if ($tmp) {
                    unset($tmp['q']);
                    $default = array_merge($default, $tmp);
                }
            }
        }
        if (isset($default['media_type'])) {
            $media_type = strtolower($default['media_type']);
            unset($default['media_type']);
        }

        $url = self::REST_API_HOST."1.1/search/tweets.json?".http_build_query($default);
        $headers = array( 
            // "GET ".$url." HTTP/1.1", 
            "Host: api.twitter.com", 
            "User-Agent: ".self::USER_AGENT,
            "Authorization: Bearer ".$bearer_token
        );
        // $output = $this->curl($url, array(), $headers, 'GET', false, true);
        $output = $this->curl($url, array(), $headers, 'GET');
        // var_dump($output);
        // @file_put_contents('./application/twitter-search-logs.txt', $default['q'].' : '.json_encode($output)."\n", FILE_APPEND);
        if (isset($output['errors'])) {
            //Bad Authentication Error
            if ((int) $output['errors'][0]['code'] === 215) {
                unset($_SESSION[$this->sessionAppkey], $default['q'], $bearer_token);
                $this->search($query, $default);
            }
            die('Error Code '.$output['errors'][0]['code'].' : '.$output['errors'][0]['message']);
        }
        
        $result = array();
        if (isset($output['statuses'])) {

            $tweet = new Tweet();
            $output['search_metadata']['result_type'] = $default['result_type'];
            if ($media_type) {
                $output['search_metadata']['media_type'] = $media_type;
                foreach ($output['statuses'] as $tw) {
                    if (isset($tw['entities']['media']) && $tw['entities']['media'][0]['type'] == $media_type) {
                        $result[] = clone $tweet->set_data($tw);
                    }
                }
            } else {
                foreach ($output['statuses'] as $tw) {
                    $result[] = clone $tweet->set_data($tw);
                }
            }

            $result = new TwitterSearchResult($result, $this);
            $result->set_metadata($output['search_metadata']);
        }

        return $result;
    }

    /**
     * 
     * @param string $screen_name
     * @param int $count
     * @param string $cursor
     * @return TwitterFollowers
     */
    public function get_followers($screen_name, $cursor = '-1', $count = 5000)
    {
        $token = $this->get_app_token();
        if (!$cursor) {
            $cursor = '-1';
        }
        $default = array(
            'screen_name' => $screen_name,
            'cursor' => $cursor,
            'count' => $count
        );
        $url = self::REST_API_HOST."1.1/followers/ids.json?".http_build_query($default);
        $headers = array( 
            // "GET ".$url." HTTP/1.1", 
            "Host: api.twitter.com", 
            "User-Agent: ".self::USER_AGENT,
            "Authorization: Bearer ".$token
        );
        $output = $this->curl($url, array(), $headers);
        // debug($output);
        $result = null;
        if (isset($output['ids'])) {
            $result = new TwitterFollowers($output['ids'], $this);
            $result->set_cursors(array(
                'next_cursor' => $output['next_cursor_str'],
                'previous_cursor' => $output['previous_cursor_str']
            ));
        }
        return $result;
    }

    /**
     * 
     * @param array $ids
     * @return TwitterUsers
     */
    public function get_users_by_id(array $ids)
    {
        $token = $this->get_app_token();
        $url = self::REST_API_HOST."1.1/users/lookup.json?include_entities=true&user_id=".implode(',', $ids);
        $headers = array( 
            // "GET ".$url." HTTP/1.1", 
            "Host: api.twitter.com", 
            "User-Agent: ".self::USER_AGENT,
            "Authorization: Bearer ".$token
        );
        $output = $this->curl($url, array(), $headers);

        $result = null;
        if ($output) {
            $result = new TwitterUsers($output, $this);
        }
        return $result;
    }

    /**
     * 
     * @param string $url
     * @param array $values
     * @param array $headers
     * @param string $method
     * @param boolean $json_decode
     * @return array
     */
    private function curl($url, $values = array(), $headers = array(), $method = 'GET', $json_decode = true, $include_response_headers = false)
    {
        if ($method === 'GET' && $values) {
            $url = rtrim($url, '?').'?'.http_build_query($values);
        }
        $ch = curl_init();  // setup a curl
        curl_setopt($ch, CURLOPT_URL, $url);  // set url to send to
        if ($headers) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); // set custom headers
        }
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return output
        if ($include_response_headers) {
            curl_setopt($ch, CURLOPT_HEADER, 1);
        }
        if ($method === 'POST') {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($values));
        }
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($ch); // execute the curl
        // $infoCh = print_r(curl_getinfo($ch), true)."\n\n";
        if (curl_errno($ch)) {
            // @file_put_contents('./application/curl-error-logs.txt', $infoCh.curl_error($ch)."\n\n", FILE_APPEND);
            return array('error' => curl_error($ch));
        }
        // @file_put_contents('./application/curl-logs.txt', (is_string($output) && empty($output) ? $infoCh.'[[ blank ]]' : $infoCh.$output)."\n\n", FILE_APPEND);
        if ($json_decode) {
            $output = json_decode($output, true);
        }
        curl_close($ch); // close the curldecode(curl_exec($ch), true); // execute the curl

        return $output;
    }

    /**
     * 
     * @param string $name
     * @param boolean $include_entities
     * @return TwitterUser
     */
    public function get_user_by_screen_name($name, $include_entities = true)
    {
        $token = $this->get_app_token();
        $url = self::REST_API_HOST."1.1/users/show.json?include_entities=".(int) $include_entities."&screen_name=".$name;
        $headers = array( 
            // "GET ".$url." HTTP/1.1", 
            "Host: api.twitter.com", 
            "User-Agent: ".self::USER_AGENT,
            "Authorization: Bearer ".$token
        );
        $output = $this->curl($url, array(), $headers);
        $result = null;
        if (!empty($output)) {
            $result = new TwitterUser();
            $result->set_data($output);
        }
        return $result;
    }

    /**
     * 
     * @param array $name
     * @param boolean $include_entities
     * @return TwitterUser
     */
    public function get_users_by_screen_name(array $names, $include_entities = true)
    {
        $token = $this->get_app_token();
        $url = self::REST_API_HOST."1.1/users/lookup.json?include_entities=".(int) $include_entities."&screen_name=".implode(',', $names);
        $headers = array( 
            // "GET ".$url." HTTP/1.1", 
            "Host: api.twitter.com", 
            "User-Agent: ".self::USER_AGENT,
            "Authorization: Bearer ".$token
        );
        $output = $this->curl($url, array(), $headers);
        $result = null;
        if (!empty($output)) {
            $result = new TwitterUsers($output, $this);
        }
        return $result;
    }

    /**
     * 
     * @param string $who
     * @param boolean $follow
     * @return boolean
     */
    public function follow($who, $follow = true)
    {
        $who = (string) $who;
        $isUserId = true;
        if (strpos($who, '@') === 0) {
            $isUserId = false;
        }

        $url = self::REST_API_HOST.'1.1/friendships/create.json';
        $params = $this->get_oauth_params();
        if ($isUserId) {
            $params['user_id'] = $who;
        } else {
            $params['screen_name'] = ltrim($who, '@');
        }
        // $params['oauth_token'] = $this->get_oauth_token();
        $params['follow'] = $follow;
    }

    /**
     * 
     * @return array
     */
    public function get_oauth_token()
    {
        if (isset($_SESSION[$this->sessionUserkey])) {
            if (!$this->oauthToken) {
                $this->oauthToken = $_SESSION[$this->sessionUserkey];
            }
            return $this->oauthToken;
        }

        $url = TwitterOAuth::get_request_url($this->key, $this->secret);
        $output = $this->curl($url, null, null, 'GET', false);
        $result = array();
        if ($output) {
            parse_str($output, $result);
        }
        $this->oauthToken = $result;
        $_SESSION[$this->sessionUserkey] = $this->oauthToken;
        return $this->oauthToken;
    }

    /**
     * 
     * @return array
     */
    public function get_user()
    {
        $params = array(
            'oauth_token' => $this->get_access_token()
        );
        if (isset($_SESSION[$this->sessionUserInfoKey])) {
            $params['user_id'] = $_SESSION[$this->sessionUserInfoKey]['user_id'];
        }
        if (!empty($params['oauth_token'])) {
            $url = TwitterOAuth::generate_url(self::REST_API_HOST.'1.1/statuses/user_timeline.json', $params, $this->key, $this->secret);
            debug($url);
            debug($this->curl($url));
        }
    }

    /**
     * 
     * @param string $oauth_verifier
     * @return array
     */
    public function get_access_token($oauth_verifier = null)
    {
        $return = isset($_SESSION[$this->sessionAccessTokenKey]) ? $_SESSION[$this->sessionAccessTokenKey] : null;
        if (!$return) {
            if (!$oauth_verifier) {
                $oauth_verifier = isset($_GET['oauth_verifier']) ? (string) $_GET['oauth_verifier'] : null;
            }
            $token = $this->get_oauth_token();
            if ($token) {
                $params = array(
                    'oauth_verifier' => $oauth_verifier,
                    'oauth_token' => $token['oauth_token']
                    // 'oauth_token_secret' => $token['oauth_token_secret']
                );

                $url = TwitterOAuth::generate_url(TwitterOAuth::ACCESS_TOKEN_URL, $params, $this->key, $this->secret);
                $result = $this->curl($url, null, null, 'GET', false);
                if ($result) {
                    parse_str($result, $return);
                    $_SESSION[$this->sessionAccessTokenKey] = isset($return['oauth_token']) ? $return['oauth_token'] : null;
                    $_SESSION[$this->sessionAccessTokenSecret] = isset($return['oauth_token_secret']) ? $return['oauth_token_secret'] : null;
                    $_SESSION[$this->sessionUserInfoKey] = array(
                        'user_id' => $return['user_id'],
                        'screen_name' => $return['screen_name']
                    );
                    $return = $_SESSION[$this->sessionAccessTokenKey];
                }
            }
        }
        
        return $return;
    }

    /**
     * 
     * @return string
     */
    public function get_oauth_secret()
    {
        return isset($_SESSION[$this->sessionAccessTokenSecret]) ? $_SESSION[$this->sessionAccessTokenSecret] : null;
    }

    /**
     * 
     * @return void
     */
    public function clear_session()
    {
        unset(
            $_SESSION[$this->sessionAppkey],
            $_SESSION[$this->sessionUserkey],
            $_SESSION[$this->sessionAccessTokenKey],
            $_SESSION[$this->sessionAccessTokenSecret],
            $_SESSION[$this->sessionUserInfoKey]
        );
    }

    /**
     * 
     * @return void|boolean
     */
    public function authorize()
    {
        $token = $this->get_oauth_token();
        if ($token) {
            header('Location: '.TwitterOAuth::get_authorize_url($token['oauth_token']));
            exit;
        }

        return false;
    }
}

