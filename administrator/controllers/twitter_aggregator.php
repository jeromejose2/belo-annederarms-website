<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Twitter_Aggregator extends CI_Controller {

	public function __construct() {

		parent::__construct();

	}

	public function twitter() {

		$this->load->library('Twitter', array(
			'key'=>'hrpbdMOa3GChQQIbxpALg',
			'secret'=>'RpWqA9JRo4yjLEJSWI34Cj93imMqpTM6faMIwvSOYI'
		));

		$params = array(
			'table'=>'tbl_hashtags',
			'where'=>'active = 1'
		);
		$hashtags = $this->mysql_queries->get_data($params);

		$options = $hashtags[0]['next_url_twitter'] ? $hashtags[0]['next_url_twitter'] : array('count'=>'100', 'media_type'=>'photo');

		$results = $this->twitter->search('#'.$hashtags[0]['hashtag'], $options);

		$registrants_saved = 0;
		$entries_saved = 0;

		foreach($results as $twits) {

			$caption = strtolower($twits->text());
			$find = strtolower('#annederarms');

			if(strpos($caption, $find) !== FALSE) {
				$data = array(
					'media_id'=>$twits->id_str,
					'user_id'=>$twits->user('id_str'),
					'full_name'=>$twits->user('name'),
					'username'=>$twits->user('screen_name'),
					'caption'=>$twits->text(),
					'user_image'=>$twits->user('profile_image_url'),
					'profile_page_link'=>'https://twitter.com/account/redirect_by_id/'.$twits->user('id_str'),
					'created_at'=>$twits->created_at,
					'source'=>'Twitter'
				);
				
				if(!$this->is_exist($data['media_id'])) {
					$params = array(
						'table'=>'tbl_entries',
						'post'=>array(
							'media_id'=>$data['media_id'],
							'twitter_user_id'=>$data['user_id'],
							'full_name'=>$data['full_name'],
							'username'=>$data['username'],
							'caption'=>$data['caption'],
							'source'=>$data['source'],
							'created_at'=>$data['created_at'],
							'user_image'=>$data['user_image']
						)
					);
					$this->mysql_queries->insert_data($params);
					$entries_saved++;
				}

				if(!$this->is_registrant($data['user_id'])) {
					$params = array(
						'table'=>'tbl_registrants',
						'post'=>array(
							'twitter_user_id'=>$data['user_id'],
							'full_name'=>$data['full_name'],
							'username'=>$data['username'],
							'profile_page_link'=>$data['profile_page_link']
						)
					);
					$this->mysql_queries->insert_data($params);
					$registrants_saved++;
				}
			}
		}
		$params = array(
			'table'=>'tbl_hashtags',
			'where'=>'active = 1',
			'post'=>array(
				'next_url_twitter'=>@$results->next_url()
			)
		);
		$this->mysql_queries->update_data($params);
		
		echo 'Query result:<br>';
		echo 'Entries saved: <b>'.$entries_saved.'</b><br>';
		echo 'Registrants saved: <b>'.$registrants_saved.'</b><br>';
		echo 'Next url: <b>'.$results->next_url().'</b>';

	}

	private function is_registrant($twitter_user_id) {

		$params = array(
			'table'=>'tbl_registrants',
			'where'=>'twitter_user_id = '.$twitter_user_id
		);

		return $this->mysql_queries->get_data($params);

	}

	private function is_exist($media_id) {

		$params = array(
			'table'=>'tbl_entries',
			'where'=>'media_id = '.$media_id
		);

		return $this->mysql_queries->get_data($params);

	}

}

?>