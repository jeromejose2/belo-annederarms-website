<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registrants extends CI_Controller {

	public function __construct() {

		parent::__construct();

		$this->table = 'tbl_registrants';

	}

	public function index() {

		$this->limit = isset( $_GET['psize'] ) ? $_GET['psize'] : 15;
		$this->curpage = $this->uri->segment(3, 1);
		$this->offset = ( $this->curpage - 1 ) * $this->limit;
		$this->paging = 3;

		/* search function */
		$this->filter = FALSE;
		$this->search_filters = '1';
		if( isset($_GET['search'] ) || isset( $_GET['filter']) ) {
			foreach( $_GET as $k => $v ) {
				if( $v != '' ) {
					$this->filter[$k] = $v;
				}
			}
			/* reset pagination by redirecting to page 1 */
			if( isset($this->filter['search']) ) {
				unset( $this->filter['search'] );
				$this->filter['filter'] = 1;
				/* here goes the reset */
				redirect('registrants/index/1'. '?' .http_build_query($this->filter, '', '&'), 'location');
			} else {
				/* add your search parameters here */
				$this->search_filters .= isset( $this->filter['id'] ) ? " AND instagram_user_id = '". $this->filter['id'] ."'" : FALSE;
				$this->search_filters .= isset( $this->filter['username'] ) ? " AND username LIKE '%". $this->filter['username'] ."%'" : FALSE;
				$this->search_filters .= isset( $this->filter['name'] ) ? " AND full_name LIKE '%". $this->filter['name'] ."%'" : FALSE;
				$this->search_filters .= isset( $this->filter['timestamp'] ) ? " AND date(timestamp) = '". $this->filter['timestamp'] ."'" : FALSE;
			}
		}
		/* end search function */

		$this->params = array(
			'table'=>$this->table,
			'where'=>$this->search_filters,
			'offset'=>$this->offset,
			'limit'=>$this->limit,
			'order'=>'timestamp DESC'
		);
		$this->data['items'] = $this->mysql_queries->get_data( $this->params );

		$this->params = array(
			'table'=>$this->table,
			'where'=>$this->search_filters
		);
		$this->total = $this->mysql_queries->get_data( $this->params );

		/* total count of data */
		$this->data['total'] = sizeof( $this->total );
		/* pagination */
		$this->data['pagination'] = $this->globals->pagination( sizeof($this->total), $this->curpage, site_url('registrants/index'), $this->curpage, $this->limit );

		$this->template['content'] = $this->load->view('registrants-content', $this->data, TRUE);
		$this->load->view('main_template', $this->template, FALSE);

	}

}