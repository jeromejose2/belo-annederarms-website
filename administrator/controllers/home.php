<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index() {

		$start_date = '2015-04-18';
		$end_date = '2015-05-17';

		$weeks_count = $this->globals->get_weeks($start_date, $end_date);

		$params = array('table'=>'tbl_registrants', 'fields'=>'count(*) as total');
		$items = $this->mysql_queries->get_data($params);

		$registrants = array();
		$registrants['total'] = $items[0]['total'];

		for($i = 0; $i < sizeof($weeks_count); $i++) {
			$params = array(
				'table'=>'tbl_registrants',
				'fields'=>'count(*) as total',
				'where'=>"date(timestamp) between '".$weeks_count[$i][0]."' AND '".$weeks_count[$i][1]."'"
			);
			$items = $this->mysql_queries->get_data($params);
			if($items) $registrants['weekly'][] = array($weeks_count[$i][0], $items[0]['total']);
			else $registrants['weekly'][] = array($weeks_count[$i][0], 0);
		}

		$params = array('table'=>'tbl_entries', 'fields'=>'count(*) as total');
		$items = $this->mysql_queries->get_data($params);

		$entries = array();
		$entries['total'] = $items[0]['total'];

		for($i = 0; $i < sizeof($weeks_count); $i++) {
			$params = array(
				'table'=>'tbl_entries',
				'fields'=>'count(*) as total',
				'where'=>"date(created_at) between '".$weeks_count[$i][0]."' AND '".$weeks_count[$i][1]."'"
			);
			$items = $this->mysql_queries->get_data($params);
			if($items) $entries['weekly'][] = array($weeks_count[$i][0], $items[0]['total']);
			else $entries['weekly'][] = array($weeks_count[$i][0], 0);
		}

		$data['registrants'] = $registrants;
		$data['entries'] = $entries;

		$this->template['content'] = $this->load->view('home', $data, TRUE);
		$this->load->view('main_template', $this->template, FALSE);

	}
}