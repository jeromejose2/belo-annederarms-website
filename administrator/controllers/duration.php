<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Duration extends CI_Controller {

	public function __construct() {

		parent::__construct();

	}

	public function index() {

		if($_POST) {
			$params = array(
				'table'=>'tbl_promo_duration',
				'post'=> $_POST
			);
			$this->mysql_queries->update_data($params);
		}

		$params = array(
			'table'=>'tbl_promo_duration'
		);
		$this->data['duration'] = $this->mysql_queries->get_data($params);	

		$this->template['content'] = $this->load->view('duration-content', $this->data, TRUE);
		$this->load->view('main_template', $this->template, FALSE);

	}

}