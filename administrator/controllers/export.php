<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Export extends CI_Controller {

 	public function __construct()	{

		parent::__construct();

	}

 	public function index() {
	}

	public function registrants() {

		/*start search function*/
		$filter = false;
		$search_filters = '1';

		if(isset($_GET['filter']) ){

			foreach($_GET as $k => $v){
				if( $v!=''){
					$filter[$k] = $v;
				}
			}
			//add the filters to 'where'(sql) statement
			$search_filters .= isset( $filter['id'] ) ? " AND instagram_user_id = '". $filter['id'] ."'" : FALSE;
			$search_filters .= isset( $filter['username'] ) ? " AND username LIKE '%". $filter['username'] ."%'" : FALSE;
			$search_filters .= isset( $filter['name'] ) ? " AND full_name LIKE '%". $filter['name'] ."%'" : FALSE;
			$search_filters .= isset( $filter['timestamp'] ) ? " AND date(timestamp) = '". $filter['timestamp'] ."'" : FALSE;
		}
		/*end search function*/

		$params = array(
			'table'=>'tbl_registrants',
			'where' => $search_filters
		);

		$params = array(
			'table'=>'tbl_registrants',
			'where'=>$search_filters
		);
		$data = $this->mysql_queries->get_data($params);

		$row  = array();

		if($data) {
				$row[] = array(
						'INSTAGRAM ID',
						'NAME',
						'USERNAME',
						'TIMESTAMP'
					);
               foreach($data as $k => $v)
			   {
					extract($v);
			   		$row[] =  array(
							$instagram_user_id,
							$full_name,
							$username,
							date('M d, Y', strtotime($timestamp))
						);
            	 }
         	}

		$this->load->library('to_excel_array');
    		$this->to_excel_array->to_excel($row, 'registrants_'.date("m-d-y"));

	}

	public function entries() {

		/*start search function*/
		$filter = false;
		$search_filters = '1';

		if(isset($_GET['filter']) ){

			foreach($_GET as $k => $v){
				if( $v!=''){
					$filter[$k] = $v;
				}
			}
			//add the filters to 'where'(sql) statement
			$search_filters .= isset( $filter['id'] ) ? " AND id = '". $filter['id'] ."'" : FALSE;
			$search_filters .= isset( $filter['name'] ) ? " AND full_name LIKE '%". $filter['name'] ."%'" : FALSE;
			$search_filters .= isset( $filter['username'] ) ? " AND username LIKE '%". $filter['tweet'] ."%'" : FALSE;
			$search_filters .= isset( $filter['created_at'] ) ? " AND date(created_at) = '". $filter['created_at'] ."'" : FALSE;
		}
		/*end search function*/

		$params = array(
			'table'=>'tbl_entries',
			'where' => $search_filters
		);

		$params = array(
			'table'=>'tbl_entries',
			'where'=>$search_filters
		);
		$data = $this->mysql_queries->get_data($params);

		$row  = array();

		if($data) {
				$row[] = array(
						'INSTAGRAM ID',
						'NAME',
						'USERNAME',
						'ENTRY LINK',
						'ENTRY DATE & TIME'
					);
               foreach($data as $k => $v)
			   {
					extract($v);
			   		$row[] =  array(
							$instagram_user_id,
							$full_name,
							$username,
							$image_url,
							date('M d, Y', strtotime($created_at))
						);
            	 }
         	}

		$this->load->library('to_excel_array');
    		$this->to_excel_array->to_excel($row, 'entries_'.date("m-d-y"));



	}

}