<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

	public function __construct() {

		parent::__construct();

	}

	public function entries_status() {

		$params = array(
			'table'=>'tbl_entries',
			'where'=>'id = '.$this->input->post('id'),
			'post'=>$this->input->post()
		);
		$this->mysql_queries->update_data($params);

	}

	public function entries_delete() {

		$params = array(
			'table'=>'tbl_entries',
			'field'=>'id',
			'value'=>$this->input->post('id')
		);
		$this->mysql_queries->delete_data($params);

	}

}