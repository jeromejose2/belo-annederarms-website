<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {

		parent::__construct();

		if( $this->session->userdata('logged_in') ) {
			redirect('home');
		}

	}

	public function index() {

		if( isset($_POST['username']) ) {
			if( !empty($_POST['username']) && !empty($_POST['password']) ) {
				$this->params = array(
					'table'=>'tbl_cms_users',
					'where'=>'username = \''.$_POST['username'].'\' and password = \''.md5($_POST['password']).'\''
				);
				$is_valid = $this->mysql_queries->get_data( $this->params );

				if( $is_valid ) {
					$session_data = array(
						'user_name'=>$is_valid[0]['username'],
						'user_id'=>$is_valid[0]['user_id'],
						'user_level'=>$is_valid[0]['level'],
						'user_account'=>$is_valid[0]['account_name'],
						'user_email'=>$is_valid[0]['email'],
						'logged_in'=>TRUE
					);
					$this->session->set_userdata($session_data);
					redirect('home', 'location');
				} else {
					$result = 'Invalid username/password.';
				}
			} else {
				$result = 'Invalid username/password';
			}
		} else {
			$result = '';
		}

		$this->data['error_message'] = $result;
		$this->load->view('login', $this->data, FALSE);

	}
}