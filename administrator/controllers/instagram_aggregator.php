<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Instagram_Aggregator extends CI_Controller {

        private $hashtags = array('ANNEderarms', 'BeloBeautyDuo');
        private $tag = '@Belo_Essentials';

        public function __construct() {

                parent::__construct();

        }

        public function fetch() {
                $registrants_saved = 0;
                $entries_saved = 0;
                $entries_processed = 0;

                $hasMore = true;

                // set the offical ANNEderarms IG client id
                $client_id = '657b8e47a03f4e70a58e1fec26b5eba8';

                $url = 'https://api.instagram.com/v1/tags/'.$this->hashtags[0].'/media/recent?client_id='.$client_id.'&count=30';

                while ($hasMore) {

                        $feed = $this->getInstagramFeed($url);
                        $results = json_decode($feed, true);

                        if (!is_array($results)) {
                                var_dump($results);
                                exit('An error occured getting the Instagram feed.');
                        }


                        foreach ($results['data'] as $post) {

                                $entries_processed++;

                                $data = array(
                                        'type' => $post['type'],
                                        'media_id' => substr($post['id'], 0, strpos($post['id'], "_")),
                                        //'media_id' => strstr($post['id'], '_', true),
                                        'tags' => $post['tags'],
                                        'created_at' => date('Y-m-d H:i:s', $post['caption']['created_time']),
                                        'username' => $post['user']['username'],
                                        'instagram_user_id' => $post['user']['id'],
                                        'full_name' => $post['user']['full_name'],
                                        'caption' => $post['caption']['text'],
                                        'image_url' => $post['link'],
                                        'image_thumbnail' => $post['images']['thumbnail']['url'],
                                        'image_standard' => $post['images']['standard_resolution']['url']
                                );

                                if ($data['type'] == 'image' && $this->validateTags($data['tags'], $data['caption']) === true) {

                                        if (!$this->is_exist($data['media_id'])) {

                                                $params = array(
                                                        'table' => 'tbl_entries',
                                                        'post' => array(
                                                                'media_id' => $data['media_id'],
                                                                'instagram_user_id' => $data['instagram_user_id'],
                                                                'full_name' => $data['full_name'],
                                                                'username' => $data['username'],
                                                                'created_at' => $data['created_at'],
                                                                'caption' => $data['caption'],
                                                                'image_url' => $data['image_url'],
                                                                'image_thumbnail' => $data['image_thumbnail'],
                                                                'image_standard' => $data['image_standard']
                                                        )
                                                );

                                                $this->mysql_queries->insert_data($params);
                                                $entries_saved++;
                                        }

                                        if (!$this->is_registrant($data['instagram_user_id'])) {
                                                $params = array(
                                                        'table' => 'tbl_registrants',
                                                        'post' => array(
                                                                'instagram_user_id' => $data['instagram_user_id'],
                                                                'full_name' => $data['full_name'],
                                                                'username' => $data['username'],
                                                                'profile_page_link' => 'https://instagram.com/'.$data['username']
                                                        )
                                                );
                                                $this->mysql_queries->insert_data($params);
                                                $registrants_saved++;
                                        }

                                }
                        }

                        if (isset($results['pagination']['next_url'])) {
                                $url = $results['pagination']['next_url'];
                        } else {
                                $hasMore = false;
                        }

                }

                echo 'Query result:<br>';
                echo 'Entries saved: <b>'.$entries_saved.'</b><br>';
                echo 'Registrants saved: <b>'.$registrants_saved.'</b><br>';
                echo 'Total entries processed: <b>'.$entries_processed.'</b><br>';

        }

        private function is_registrant($twitter_user_id) {

                $params = array(
                        'table'=>'tbl_registrants',
                        'where'=>'instagram_user_id = '.$twitter_user_id
                );

                return $this->mysql_queries->get_data($params);

        }

        private function is_exist($media_id) {

                $params = array(
                        'table'=>'tbl_entries',
                        'where'=>'status = 1 AND media_id = '.$media_id
                );

                return $this->mysql_queries->get_data($params);

        }

        private function getInstagramFeed($url) {

                $ch = curl_init();
                curl_setopt_array($ch, array(
                        CURLOPT_URL => $url,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_SSL_VERIFYPEER => false,
                        CURLOPT_SSL_VERIFYHOST => 2
                ));

                $result = curl_exec($ch);
                curl_close($ch);
                return $result;
        }

        private function validateTags($tags, $caption) {

                foreach ($this->hashtags as $hashtag) {
                        if (in_array(strtolower($hashtag), array_map('strtolower', $tags)) === false)
                                return false;
                }

                if (stripos($caption, $this->tag) === false) {
                        return false;
                }

                return true;
        }

}

?>
