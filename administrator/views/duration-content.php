     <div class="container main-content">
          <div class="page-header">
               <h3>Promo Duration</h3>
          </div>
          
          <form class="form-horizontal" role="form" action="<?= site_url('duration') ?>" method="POST">                               
               <div class="container">
               	<div class="form-group row">
               		<label class="control-label col-lg-1">Start Date</label>
               		<div class="col-lg-3">
                   		<input type="text" class="form-control date" name="start_date" value="<?= $duration[0]['start_date'] ?>">
                   	</div>
                </div>
                
                 <div class="form-group row">
                	<label class="control-label col-lg-1">End Date</label>
                	<div class="col-lg-3">
                		<input type="text" class="form-control date" name="end_date" value="<?= $duration[0]['end_date'] ?>">
                	</div>
                   
                </div>
               
                <div class="form-group row">
                	<label class="control-label">End Promo Message</label>
                    <textarea name="end_message" class="form-control"><?= $duration[0]['end_message'] ?></textarea>
                </div>
               </div>

               <div style="padding-bottom: 25px">
                    <input type="submit" class="btn" value="Update">
               </div>
          </form>

     </div>

<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/admin/css/jquery-te-1.4.0.css">
<script type="text/javascript" src="<?= base_url() ?>assets/admin/js/jquery-te-1.4.0.min.js"></script>
<script type="text/javascript">
	$('.date').datepicker({
    	dateFormat:'yy-mm-dd'
 	});  
     
    $('textarea').jqte();
</script>