<div class="container main-content">
     <div class="page-header">
          <h3>Accounts <span class="badge"><?php echo $total;?></span></h3>

          <div class="actions">
               <a href="javascript:void(0)" class="btn btn-primary" data-toggle="modal" data-target="#popup-create">New Account</a>
          </div>
     </div>

     <table class="table table-bordered">
          <thead>
               <tr>
                    <th>UserID</th>
                    <th>Name</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Action</th>
               </tr>
          </thead>
          <tbody>
               <?php if($items): ?>
                    <?php foreach($items as $key => $v): ?>
                         <tr id="row-<?php echo $v['user_id'];?>">
                              <td><?php echo $v['user_id'];?></td>
                              <td><?php echo $v['account_name'];?></td>
                              <td><?php echo $v['username'];?></td>
                              <td><?php echo $v['email'];?></td>
                              <td style="width: 1%">
                                   <?php if($this->session->userdata('user_level') == 1 || $v['level'] == 2): ?>
                                        <a href="javascript:void(0)" style="margin-right: 10px" title="Edit" data-toggle="modal" data-target="#popup-<?php echo $v['user_id'];?>"><i class="glyphicon glyphicon-edit"></i></a>
                                        <a href="javascript:void(0)" title="Delete" class="delete" data-id="<?php echo $v['user_id'];?>"><i class="glyphicon glyphicon-remove-circle"></i></a>
                                   <?php endif; ?>
                              </td>
                         </tr>

                         <!-- MODALS -->
                              <div class="modal fade" id="popup-<?php echo $v['user_id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                      <h4 class="modal-title" id="myModalLabel">Update Account</h4>
                                    </div>
                                    <div class="modal-body">
                                      <!-- MODAL CONTENT -->

                                        <form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('accounts/update')?>">
                                             <input type="hidden" name="user_id" value="<?php echo $v['user_id'];?>">
                                             
                                                  <label class="control-label">Name</label>
                                                  <div>
                                                       <input type="text" class="form-control" name="account_name" value="<?php echo $v['account_name'];?>">
                                                  </div>
                                             


                                             
                                                  <label class="control-label">Email</label>
                                                  <div>
                                                       <input type="text" class="form-control" name="email" value="<?php echo $v['email'];?>">
                                                  </div>
                                             

                                             
                                                  <label class="control-label">Username</label>
                                                  <div>
                                                       <input type="text" class="form-control" name="username" value="<?php echo $v['username'];?>">
                                                  </div>
                                             

                                             
                                                  <label class="control-label">New Password</label>
                                                  <div>
                                                       <input type="text" class="form-control" name="password" placeholder="Leave empty to keep old password">
                                                  </div>
                                             

                                             
                                                  <label class="control-label">Type</label>
                                                  <div>
                                                       <select name="level" class="form-control">
                                                            <?php if($this->session->userdata('user_level') == 1): ?>
                                                                 <option value="1" <?php echo isset($v['level']) && $v['level'] == 1 ? 'selected' : ''?>>Developer (Full access)</option>
                                                            <?php endif; ?>
                                                            <option value="2" <?php echo isset($v['level']) && $v['level'] == 2 ? 'selected' : ''?>>Admin</option>
                                                            <option value="3" <?php echo isset($v['level']) && $v['level'] == 3 ? 'selected' : ''?>>Viewer</option>
                                                       </select>
                                                  </div>
                                             

                                      <!-- END MODAL CONTENT -->
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-primary">Save changes</button>
                                        </form>

                                    </div>
                                  </div>
                                </div>
                              </div>
                         <!-- END MODALS -->
                    <?php endforeach; ?>
               <?php else: ?>
                    <tr>
                         <td colspan="15"><center style="color: red">No data found</center></td>
                    </tr>
               <?php endif;?>
          </tbody>
     </table>

</div>

<!-- NEW ACCOUNT -->
<!-- MODALS -->
     <div class="modal fade" id="popup-create" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
       <div class="modal-dialog">
         <div class="modal-content">
           <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
             <h4 class="modal-title" id="myModalLabel">New Account</h4>
           </div>
           <div class="modal-body">
             <!-- MODAL CONTENT -->

     <form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('accounts/create')?>">
          
               <label class="control-label">Name</label>
               <div>
                    <input type="text" class="form-control" name="account_name">
               </div>
          

          
               <label class="control-label">Email</label>
               <div>
                    <input type="text" class="form-control" name="email">
               </div>
          

          
               <label class="control-label">Username</label>
               <div>
                    <input type="text" class="form-control" name="username">
               </div>
                    

          
               <label class="control-label">Password</label>
               <div>
                    <input type="text" class="form-control" name="password">
               </div>
          

          
               <label class="control-label">Type</label>
               <div>
                    <select name="level" class="form-control">
                         <?php if($this->session->userdata('user_level') == 1): ?>
                              <option value="1">Developer (Full access)</option>
                         <?php endif; ?>
                         <option value="2">Admin</option>
                         <option value="3">Viewer</option>
                    </select>
               </div>
          
          </div>
           <div class="modal-footer">
             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
             <button type="submit" class="btn btn-primary">Create</button>
               </form>

           </div>
         </div>
       </div>
     </div>

<!-- END NEW ACCOUNT -->

<script>
     $(function(){
          var lytebox = new Lytebox;
          $('.delete').on('click', function(){
               var id = $(this).data('id');
               lytebox.dialog({
                    message: 'Are you sure you want to delete this?', 
                    title : 'Confirm<hr style="margin: 5px 0; border-color: #ccc">',
                    type : 'confirm',
                    onConfirm : function(){
                         $.post('<?php echo site_url("accounts/delete")?>', {id:id}, function(){
                              $('#row-'+id).hide();
                         });
                    },
                    top: 100
               });
          });
     });
</script>