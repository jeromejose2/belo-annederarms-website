     <div class="container main-content">

          <div class="page-header">

               <h3>Registrants <span class="badge"><?= @$total ?></span></h3>



               <div class="actions">

                    <a class="btn btn-primary advanced"><i class="glyphicon glyphicon-search"></i> <span class="hidden-phone"> Advance Search</span></a>

               </div>

               <div class="actions" style="margin-right: 1%">

                    <a class="btn btn-primary" href="<?=site_url('export/registrants?'.http_build_query($_GET, '', "&"))?>"><i class="glyphicon glyphicon-search"></i> <span class="hidden-phone"> Export</span></a>

               </div>


          <div class="advance-search">

               <div class="form-content">

                    <form class="form-search">

                         <div class="input-prepend">

                              <span class="add-on">ID</span>

                              <input type="text" class="input-medium" name="id" value="<?=isset($_GET['id']) ? $_GET['id'] : '' ?>" >

                         </div>

                         <div class="input-prepend">

                              <span class="add-on">USERNAME</span>

                              <input type="text" class="input-medium" name="username" value="<?=isset($_GET['username']) ? $_GET['username'] : '' ?>" >

                         </div>

                         <div class="input-prepend">

                              <span class="add-on">NAME</span>

                              <input type="text" class="input-medium" name="name" value="<?=isset($_GET['name']) ? $_GET['name'] : '' ?>" >

                         </div>

                         <div class="input-prepend">

                              <span class="add-on">DATE</span>

                              <input type="text" class="input-medium" name="timestamp" value="<?=isset($_GET['timestamp']) ? $_GET['timestamp'] : '' ?>" autocomplete="off">

                         </div>

                         <div class="input-prepend">

                              <span class="add-on">ITEMS/PAGE</span>

                              <select name="psize" class="input-small">

                                   <option <?=isset($_GET['psize']) && $_GET['psize']=='15'  ? 'selected="selected"' : ''?>>15</option>

                                   <option <?=isset($_GET['psize']) && $_GET['psize']=='30'  ? 'selected="selected"' : ''?>>30</option>

                                   <option <?=isset($_GET['psize']) && $_GET['psize']=='50'  ? 'selected="selected"' : ''?>>50</option>

                                   <option <?=isset($_GET['psize']) && $_GET['psize']=='100' ? 'selected="selected"' : ''?>>100</option>

                                   <option <?=isset($_GET['psize']) && $_GET['psize']=='200' ? 'selected="selected"' : ''?>>200</option>

                              </select>

                         </div> 



                         <button type="submit" class="btn btn-primary" name="search" value="1">Search</button>

                    </form>

               </div>

          </div>

          </div>



          <table class="table table-bordered">

               <thead>

                    <tr>

                         <th>#</th>

                         <th>Twitter ID</th>

                         <th>Name</th>

                         <th>Username</th>

                         <th>Timestamp</th>

                    </tr>

               </thead>

               <tbody class="row">

                    <? if($items): ?>

                         <? foreach($items as $k => $v): ?>

                              <tr id="item-<?= $v['id'] ?>">

                                   <td><?= $v['id'] ?></td>

                                   <td><?= $v['twitter_user_id'] ?></td>

                                   <td><a href="<?= $v['profile_page_link'] ?>" target="_blank"><?= $v['full_name'] ?></a></td>

                                   <td><?= $v['username'] ?></td>

                                   <td><?= $v['timestamp'] ?></td>

                              </tr>

                         <? endforeach; ?>

                    <? else: ?>

                         <tr>

                              <td colspan="10" style="text-align: center">No Result</td>

                         </tr>

                    <? endif; ?>

               </tbody>

          </table>



          <?= $pagination ?>



     </div>



<script>

     $(document).ready(function(){

          $('.advanced').click(function(){

               $('.advance-search').slideToggle();

          });



          $('input[name="timestamp"]').datepicker({

             dateFormat:'yy-mm-dd'

          });               

     });     

</script>