<!DOCTYPE html>
<html>
<head>
     <meta charset="UTF-8">
     <title>CMS</title>
     <!-- Bootstrap -->
     <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/bootstrap.min.css" >
     <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/style.css" >
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
     <!--[if lt IE 9]>
     <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
     <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
     <![endif]-->
     <link rel="stylesheet" href="<?php echo base_url()?>assets/admin/css/lytebox.css">
     <link rel="stylesheet" href="<?= base_url() ?>assets/admin/css/jquery-ui.min.css">
     <script src="<?php echo base_url()?>assets/admin/js/lytebox.2.3.js"></script>
     <script src="<?php echo base_url()?>assets/admin/js/jquery.js"></script>
     <script src="<?= base_url() ?>assets/admin/js/jquery-ui.js"></script>
     <script>
          var lytebox = new Lytebox;
     </script>
</head>
<body>

     <!--start header-->
     <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo site_url()?>">NuWorks</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <ul class="nav navbar-nav">
                    <li class="<?php echo $this->uri->segment(1) == 'home' ? 'active' : ''?>"><a href="<?php echo site_url('home')?>">Dashboard</a></li>
                    <li class="<?php echo $this->uri->segment(1) == 'registrants' ? 'active' : ''?>"><a href="<?= site_url('registrants') ?>">Registrants</a></li>
                    <li class="<?php echo $this->uri->segment(1) == 'entries' ? 'active' : ''?>"><a href="<?= site_url('entries') ?>">Entries</a></li>
                    <li class="<?php echo $this->uri->segment(1) == 'mechanics' ? 'active' : ''?>"><a href="<?= site_url('mechanics') ?>">Mechanics</a></li>
                    <li class="<?php echo $this->uri->segment(1) == 'duration' ? 'active' : ''?>"><a href="<?= site_url('duration') ?>">Promo Duration</a></li>
                    <!-- <li>
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dopdown <b class="caret"></b></a>
                         <ul class="dropdown-menu">
                              <li><a href="#">Item</a></li>
                              <li><a href="#">Item</a></li>
                              <li><a href="#">Item</a></li>
                              <li class="divider"></li>
                              <li><a href="#">Item</a></li>
                         </ul>
                    </li> -->
               </ul>

               <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown <?php echo $this->uri->segment(1) == 'accounts' ? 'active' : '' ?>">
                         <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Settings <b class="caret"></b></a>
                         <ul class="dropdown-menu">
                              <li><a href="<?php echo site_url('accounts')?>">Accounts</a></li>
                              <li class="divider"></li>
                              <li><a href="<?php echo site_url('logout')?>">Logout</a></li>
                         </ul>
                    </li>
               </ul>
          </div><!-- /.navbar-collapse -->
     </nav>
     <!--end header-->


     <!--start main content -->
     <?php echo $content;?>
     <!--end main content -->

     <!--start footer -->
     <footer class="main">
          <div class="container">
               <small>Nuworks Interactive Labs &copy; 2014</small>
          </div>
     </footer>
     <!--end footer -->


     <script src="<?php echo base_url()?>assets/admin/js/bootstrap.min.js"></script>
</body>
</html>