     <div class="container main-content">

          <div class="page-header">

               <h3>Entries <span class="badge"><?= @$total ?></span></h3>



               <div class="actions">

                    <a class="btn btn-primary advanced"><i class="glyphicon glyphicon-search"></i> <span class="hidden-phone"> Advance Search</span></a>

               </div>

               <div class="actions" style="margin-right: 1%">

                    <a class="btn btn-primary" href="<?=site_url('export/entries?'.http_build_query($_GET, '', "&"))?>"><i class="glyphicon glyphicon-search"></i> <span class="hidden-phone"> Export</span></a>

               </div>



          <div class="advance-search">

               <div class="form-content">

                    <form class="form-search">

                         <div class="input-prepend">

                              <span class="add-on">ID</span>

                              <input type="text" class="input-medium" name="id" value="<?=isset($_GET['id']) ? $_GET['id'] : '' ?>" >

                         </div>

                         <div class="input-prepend">

                              <span class="add-on">NAME</span>

                              <input type="text" class="input-medium" name="name" value="<?=isset($_GET['name']) ? $_GET['name'] : '' ?>" >

                         </div>

                         <div class="input-prepend">

                              <span class="add-on">TWEET</span>

                              <input type="text" class="input-medium" name="tweet" value="<?=isset($_GET['tweet']) ? $_GET['tweet'] : '' ?>" >

                         </div>

                         <div class="input-prepend">

                              <span class="add-on">SOURCE</span>

                              <select name="source">

                                 <option value=""></option>

                                 <option <?= isset($_GET['source']) && $_GET['source'] == 'Twitter' ? 'selected' : '' ?>>Twitter</option>

                                 <option <?= isset($_GET['source']) && $_GET['source'] == 'Microsite' ? 'selected' : '' ?>>Microsite</option>

                              </select>

                         </div>

                         <div class="input-prepend">

                              <span class="add-on">DATE</span>

                              <input type="text" class="input-medium" name="timestamp" value="<?=isset($_GET['timestamp']) ? $_GET['timestamp'] : '' ?>" autocomplete="off">

                         </div>

                         <div class="input-prepend">

                              <span class="add-on">ITEMS/PAGE</span>

                              <select name="psize" class="input-small">

                                   <option <?=isset($_GET['psize']) && $_GET['psize']=='15'  ? 'selected="selected"' : ''?>>15</option>

                                   <option <?=isset($_GET['psize']) && $_GET['psize']=='30'  ? 'selected="selected"' : ''?>>30</option>

                                   <option <?=isset($_GET['psize']) && $_GET['psize']=='50'  ? 'selected="selected"' : ''?>>50</option>

                                   <option <?=isset($_GET['psize']) && $_GET['psize']=='100' ? 'selected="selected"' : ''?>>100</option>

                                   <option <?=isset($_GET['psize']) && $_GET['psize']=='200' ? 'selected="selected"' : ''?>>200</option>

                              </select>

                         </div> 



                         <button type="submit" class="btn btn-primary" name="search" value="1">Search</button>

                    </form>

               </div>

          </div>

          </div>



          <table class="table table-bordered">

               <thead>

                    <tr>

                         <th>#</th>

                         <th>Name</th>

                         <th>Source</th>

                         <th>Tweet</th>

                         <th style="width: 150px">Action</th>

                    </tr>

               </thead>

               <tbody class="row">

                    <? if($items): ?>

                         <? foreach($items as $k => $v): ?>

                              <tr id="item-<?= $v['id'] ?>">

                                   <td><?= $v['id'] ?></td>

                                   <td><?= $v['full_name'] ?></td>

                                   <td><?= $v['source'] ?></td>

                                   <td><?= $v['caption'] ?></td>

                                   <td>

                                        <div class="btn-group btn-group-xs action-<?= $v['id'] ?>" alt="<?= $v['id'] ?>">

                                             <button type="button" class="btn btn-default show-<?= $v['id'] ?> <?= $v['status'] == 1 ? 'btn-success' : '' ?>" onclick="_show(<?= $v['id'] ?>)">Show</button>

                                             <button type="button" class="btn btn-default hide-<?= $v['id'] ?> <?= $v['status'] == 2 ? 'btn-success' : '' ?>" onclick="_hide(<?= $v['id'] ?>)">Hide</button>

                                             <button type="button" class="btn btn-default" onclick="_delete(<?= $v['id'] ?>)">Delete</button>

                                        </div>

                                   </td>

                              </tr>

                         <? endforeach; ?>

                    <? else: ?>

                         <tr>

                              <td colspan="10" style="text-align: center">No Result</td>

                         </tr>

                    <? endif; ?>

               </tbody>

          </table>



          <?= $pagination ?>



     </div>



<script>

     $(document).ready(function(){

          $('.advanced').click(function(){

               $('.advance-search').slideToggle();

          });



          $('input[name="timestamp"]').datepicker({

             dateFormat:'yy-mm-dd'

          });               

     });     



     function _show(id) {

          var hide = $('.hide-'+id);
          var show = $('.show-'+id);
          var id = show.parent().attr('alt');
          var data = { id : id, status : 1 }

          lytebox.dialog({

               message: 'Are you sure you want to show this item?',
               type: 'confirm',
               top: 150,
               onConfirm: function(){

                    if(hide.hasClass('btn-success')) {
                         hide.removeClass('btn-success');
                    }
                    if(!show.hasClass('btn-success')) {
                         show.addClass('btn-success');
                    }

                    $.post("<?= site_url('ajax/entries_status') ?>", data);

               }

          });          

     }


     function _hide(id) {

          var hide = $('.hide-'+id);
          var show = $('.show-'+id);
          var id = show.parent().attr('alt');
          var data = { id : id, status : 2 }

          lytebox.dialog({

               message: 'Are you sure you want to hide this item?',
               type: 'confirm',
               top: 150,
               onConfirm: function(){

                    if(show.hasClass('btn-success')) {
                         show.removeClass('btn-success');
                    }
                    if(!hide.hasClass('btn-success')) {
                         hide.addClass('btn-success');
                    }

                    $.post("<?= site_url('ajax/entries_status') ?>", data);

               }

          });

     }


     function _delete(id) {

          var data = { id : id };

          lytebox.dialog({

               message: 'Are you sure you want to delete this item?',
               type: 'confirm',
               top: 150,
               onConfirm: function(){

                    $.post("<?= site_url('ajax/entries_delete') ?>", data, function(){
                         $('#item-'+id).remove();
                    });

               }

          });

     }

</script>