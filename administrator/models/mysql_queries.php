<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mysql_Queries extends CI_Model {
	 
 	public function __construct() {

		parent::__construct();

		define('BASE_URL', base_url());
		define('URI', $this->uri->segment(1));

	}

	public function get_data($params) {

		$table = isset($params['table']) ? $params['table'] : "";
		$fields = isset($params['fields']) ? $params['fields'] : "*";
		$join	= isset($params['join']) ?  $params['join'] : "";
		$where = isset($params['where']) ? "WHERE " . $params['where'] : "";
		$order = isset($params['order']) ? "ORDER BY " . $params['order'] : "";
		$group = isset($params['group']) ? "GROUP BY " . $params['group'] : "";
		$limit = (isset($params['offset']) && isset($params['limit'])) ? "LIMIT  " . $params['offset'] . ", " . $params['limit'] : "";
		$res = $this->db->query("SELECT " . $fields .
								" FROM " . $table . 
								" " . $join .
								" " . $where .
								" " . $group . 
								" " . $order . 
								" " . $limit);

		return $res->result_array();
		
	}
	
	public function insert_data($params) {

		$query = "INSERT INTO " . $params['table'] . " SET ";

		foreach($params['post'] as $k => $v) {
			$query .= $k . " = '" . mysql_real_escape_string($v) . "', ";
		}

		$query = substr($query, 0, strlen($query) - 2);
		$this->db->query($query);

		return $this->db->insert_id();

	}

	public function delete_data($params) {

		$this->db->query("DELETE FROM " . $params['table'] . " WHERE " . $params['field'] . " = " . $params['value']);

	}
	
	public function update_data($params) {

		$query = "UPDATE  " . $params['table'] . " SET ";

		foreach($params['post'] as $k => $v) {
			$value = !empty($v) ? "'" . mysql_real_escape_string($v) . "'" : 'NULL';
			$query .= $k . " = " . $value . ", ";
		}

		$query = substr($query, 0, strlen($query) - 2);
		if (isset($params['where'])) {
			$query .= " WHERE " . $params['where'];
		}
		
		$this->db->query($query);	

	}

	public function analytics() {

		$params = array(
				'table'=>'tbl_settings',
				'where'=>'type = \'analytics\''
			);
		$this->result = $this->mysql_queries->get_data($params);

		return $this->result[0]['content'];

	}

}