-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 10, 2015 at 04:53 PM
-- Server version: 5.5.36
-- PHP Version: 5.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `annederarms2`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cms_users`
--

CREATE TABLE IF NOT EXISTS `tbl_cms_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_name` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '2',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_cms_users`
--

INSERT INTO `tbl_cms_users` (`user_id`, `account_name`, `username`, `password`, `email`, `level`, `timestamp`) VALUES
(1, 'Developer', 'dev', 'e77989ed21758e78331b20e477fc5582', 'developer@nuworks.ph', 1, '2014-06-30 11:42:16'),
(3, 'ANNEderarms Admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'annederarms_admin@beloessentia', 2, '2014-08-15 07:24:06'),
(4, 'dev2', 'dev2', 'f77648e5d3e027222417fcba0f7291cb', 'dev2@creatinginfo.com', 1, '2015-04-07 09:28:19');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_entries`
--

CREATE TABLE IF NOT EXISTS `tbl_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_id` bigint(20) NOT NULL,
  `instagram_user_id` bigint(20) NOT NULL,
  `full_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `image_url` text COLLATE utf8_unicode_ci NOT NULL,
  `image_standard` text COLLATE utf8_unicode_ci NOT NULL,
  `image_thumbnail` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(2) NOT NULL DEFAULT '1' COMMENT '1=show, 2=hide',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `caption` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_entries_old`
--

CREATE TABLE IF NOT EXISTS `tbl_entries_old` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_id` bigint(20) NOT NULL,
  `twitter_user_id` bigint(20) NOT NULL,
  `full_name` varchar(128) NOT NULL,
  `username` varchar(128) NOT NULL,
  `user_image` text NOT NULL,
  `caption` text NOT NULL,
  `source` varchar(50) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '1' COMMENT '1=show, 2=hide',
  `created_at` varchar(128) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hashtags`
--

CREATE TABLE IF NOT EXISTS `tbl_hashtags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hashtag` varchar(128) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `next_url_twitter` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_hashtags`
--

INSERT INTO `tbl_hashtags` (`id`, `hashtag`, `active`, `status`, `next_url_twitter`, `timestamp`) VALUES
(1, 'kilikiliglines', 1, 1, '', '2014-08-07 06:02:02');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_promo_duration`
--

CREATE TABLE IF NOT EXISTS `tbl_promo_duration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `end_message` longtext COLLATE utf8_unicode_ci,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_promo_duration`
--

INSERT INTO `tbl_promo_duration` (`id`, `start_date`, `end_date`, `end_message`, `timestamp`) VALUES
(1, NULL, NULL, NULL, '2015-04-10 05:35:08');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_registrants`
--

CREATE TABLE IF NOT EXISTS `tbl_registrants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `instagram_user_id` bigint(20) NOT NULL,
  `full_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `profile_page_link` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `id_2` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_registrants_old`
--

CREATE TABLE IF NOT EXISTS `tbl_registrants_old` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `twitter_user_id` bigint(20) NOT NULL,
  `full_name` varchar(128) NOT NULL,
  `username` varchar(128) NOT NULL,
  `profile_page_link` varchar(128) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settings`
--

CREATE TABLE IF NOT EXISTS `tbl_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(128) NOT NULL,
  `content` longtext NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_settings`
--

INSERT INTO `tbl_settings` (`id`, `type`, `content`, `timestamp`) VALUES
(1, 'mechanics', '<b>PROMOTION DESCRIPTION</b><br>The Belo Essentials #ANNEderarms #KiliKiligLines contest is part of the second phase for the ANNEderarms campaign. Users will just have to follow @Belo_Essentials on Twitter, tweet their pick-up line for ANNEderarms which must include the hashtags #ANNEderarms and #KiliKiligLines and also tag @Belo_Essentials. Promo will run for two weeks, August 18- 31, 2014, and will be supported by radio stations The Morning Rush and Boys Night Out. Ten winners will be chosen at the end of the contest and winning entries will be featured on the Belo digital billboard from September 7-13, 2014.<br><br><b>PROMOTION PERIOD/ DURATION</b><br>Promotion period will be from August 18-31, 2014.&nbsp;<br><br><b>PROMO MECHANICS</b><br><ol><li>QUALIFICATIONS/DISQUALIFICATIONS OF PARTICIPANTS:</li><ol><li>The promotion is open to residents of the Philippines, at least eighteen (18) years of age at the time of joining, and has a valid and active Twitter account.</li><li>Employees of Intelligent Skin Care Inc. and its third party organizing agency (the organizers of the promotion), their agencies, their affiliates, subsidiaries, and their relatives up to the third degree of consanguinity or affinity may join but are NOT eligible to win in the promotion.</li><li>Participation in the promotion is a confirmation of a voluntary act by and full consent of the participant.</li><li>By joining the promotion, participants agree to grant the organizers of the promotion the ownership and the right to use or publish the names and entries at any time in its promo announcements, Website, Facebook page, Twitter page, Instagram account, and/or advertising materials, without need of compensation.</li><li>By joining the contest, organizers of the promotion may, without further compensation to them: (i) reproduce, distribute, adapt, modify, make available and/or communicate to the public, exhibit, or broadcast any material or information regarding this promotion by any means or media without restriction of any kind as to quantity, purpose or time, whether commercial or otherwise, or to any country or territory in the world; and (ii) authorize any of the above activities;</li></ol></ol><p><b>HOW TO PARTICIPATE:</b><br></p><ol><li>Those qualified and interested to join must search and follow the directions for joining located in the microsite hosted by the Belo Essentials wesbite (www.ANNEderarms.com)</li><li>Inside the Belo Essentials ANNEderarms Microsite (hereon to be referred to as Promotion), a qualified and interested person (the Participant) can submit their entries through two ways:</li><ol><li>Direct tweet via the microsite:</li><ol><li>The Tweet button will be available in the Home page.</li><li>When the user clicks on the Tweet button, user will be asked to follow the Belo Essentials Twitter account first. After liking, user can then proceed to submitting her entry.</li><li>Included in the pop-up are already the hashtags #ANNEderarms and #KiliKiligLines, and the Belo Essentials Twitter account, @Belo_Essentials</li><li>At the end of the upload process, a Thank You pop-up will appear and will inform user that his/her entry will be subject for approval.</li><li>Users can upload multiple entries as long as they follow the rules and regulations of the promotion.</li><li>All submitted entries will automatically be aggregated into the Twitter feed.</li></ol><li>Personal Twitter account:</li><ol><li>User can send their entries by following @Belo_Essentials on Twitter.</li><li>All entries must include their pick-up line, the hashtags #ANNEderarms and #KiliKiligLines, and must also tag @Belo_Essentials.</li><li>Only the tweets from public accounts will be pulled by the Content Management System.</li><li>Users can upload multiple entries as long as they follow the rules and regulations of the promotion.</li><li>All submitted entries will automatically be aggregated into the Twitter feed.</li><li>The CMS will also pull the name/username of the person who submitted.</li></ol></ol><li>Deadline of submission of entries is on August 31, 2014 11:59:59pm.</li><li>To ensure fairness of the selection process, Intelligent Skin Care, Inc. will be monitoring the validity of registrants and their entries. Valid entries are as follows:</li><ol><li>The Participant must tweet his/her creative pick-up line entry for #ANNEderarms. Entries should not use language that is impolite, malicious or offensive. Entries must also not contain or show any other brand aside from Belo Essentials.</li><li>Participants are required to include the hashtags #ANNEderarms and #KiliKiligLines, and must tag @Belo_Essentials.</li></ol><li>Organizers reserve the right to disqualify users who are found to use fake and or duplicated accounts.</li><li>Organizers have the prerogative to remove any invalid entries and/or offensive comments/entries. Intelligent Skin Care, Inc. shall disqualify and remove entries that are deemed to have used cheats; have manipulated or modified the application parameters; or have resorted to unauthorized access. Intelligent Skin Care, Inc. shall also delete/remove comments/entries that are deemed to be abusive, defamatory, obscene or in violation of generally acceptable decorum.</li><li>By joining this promotion, Participant warrants and certifies that he/she has read the full mechanics of the Promotions and that he/she agrees to abide by the terms and conditions set forth in the promotion mechanics. Failure to agree to and abide by the terms and conditions set forth in the mechanics shall disqualify such Participant from participating in this promotion.</li></ol><b>HOW TO WIN:</b><br><ol><li>Video entries will be judged according to the following criteria:</li><ol><li>40% - Creativity and uniqueness of entry</li><ol><li>How creative and unique is my pick-up line?</li></ol><li>40% - Relation of entry to #ANNEderarms</li><ol><li>Is my pick-up line related to #ANNEderarms?</li></ol><li>20% - Audience impact</li><ol><li>Is my pick-up line really a #KiliKiligLine?</li></ol></ol><li>Entries will be judged by the following:</li><ol><li>Lerma Mendoza - Marketing Head of Belo Essentials, Intelligent Skin Care, Inc.</li><li>Joyce delos Reyes - Brand Manager of Belo Essentials, Intelligent Skin Care, Inc.</li><li>Inna Villa- Account Executive of organizing agency</li></ol><li>At the end of the promotion, Intelligent Skin Care will select 10 (TEN) winners from all the entries submitted for the promotion. The announcement date will be during the first week of September, September 1-7, 2014 which will be posted on the Belo Essentials Facebook page (http://facebook.com/beloessentials), Belo Essentials Twitter account (http://twitter.com/belo_essentials), and Belo Essentials Instagram account (http://instagram.com/belo_essentials). These entries will also be featured on a Belo digital billboard along EDSA (Guadalupe) from September 7-13, 2014.</li><li>Entries will be judged by representatives from Intelligent Skin Care, Inc. and its third party organizing agency, along with an FDA representative to ensure fairness and legitimacy of the selection of winners.</li></ol><p><b>PROCEDURE IN THE NOTIFICATION OF WINNERS:</b><br></p><ol><li>Once the winners are chosen and verified, they will be notified via confirmation e-mail or via mobile phone call and will be given instructions on how to claim the prizes.</li><li>For Metro Manila winners, the winners must claim their prizes in the office of Intelligent Skin Care, Inc. located at Room 605 Ecoplaza Building 2305 Don Chino Roces Extn., Makati City, Phils. and bring the following requirements:</li><ol><li>Two (2) valid government-issued IDs with pictures (such as a School ID, Drivers License, Voters ID, SSS, or Passport)</li><li>A copy of the email with instructions on how to claim prizes</li><li>A copy of the confirmation email</li></ol><li>Claiming of prizes will be from September 8 to October 31, 2014, weekdays from 10:00am to 4:00pm only. All prizes unclaimed after this period shall be forfeited with FDAs approval.</li><li>Winners wil have to inform Intelligent Skin Care, Inc. of the date they are claiming the prize before going to the office.</li><li>For Outside Metro Manila winners, the prizes will be delivered via courier service care of Intelligent Skin Care, Inc.</li><li>A proxy can be sent for claiming of prizes provided that he/she brings an authorization letter and the necessary requirements of the winner mentioned above.</li></ol><b>OTHER GUIDELINES:&nbsp;</b><br><ol><li>By registering and joining in this promotion, registrant/qualified participant confirms, agrees and acknowledges that:</li><ol><li>Organizers of the promotion have the right to identify them as participants or winners (if applicable) in the promotion and communicate the same at any time and in any territory through any medium of release (mass media, digital media, etc.);</li><li>Participant shall hold organizers of the promotion free and harmless from any and all claims, suits and actions for damages or liabilities that may be brought by other persons, natural or juridical, in connection with his/her participation in this promotion, as well as the statements and declarations made by him/her in relation to the promotion; and</li><li>articipant shall also hold organizers of the promotion free and harmless from any and all claims arising from any damage or injury he/she may incur as a result of this promotion, acknowledging that he/she has decided to participate in the promotion under his own free will and despite any attendant risks.</li><li>Participant shall make no reproduction of any part of this website for purposes of selling or distributing the same for commercial gain nor shall he/she modify or incorporate any part of the website in any other work, publication or website.</li></ol><li>Organizers of the promotion assume no responsibility for:</li><ol><li>Any statement, comments or declarations made or submitted by the participants or members of the Belo Essentials ANNEderarms KiliKiligLines microsite. Said statements, comments or declarations are those of the participants or members of the ANNEderarms KiliKiligLines microsite and not of the organizers of the promotion.</li></ol></ol><b>PRIZES TO BE WON:&nbsp;</b><br>At the end of the promotion, will select 10 (TEN) winners from all the entries submitted for the promotion. The announcement date will be during the first week of September, September 1-7, 2014 which will be posted on the Belo Essentials Facebook page (http://facebook.com/beloessentials), Belo Essentials Twitter account (http://twitter.com/belo_essentials), and Belo Essentials Instagram account (http://instagram.com/belo_essentials).&nbsp;<br><br>Each of the ten winners will receive the following prizes:<br><ol><li>#ANNEderarm product kit</li><li>Php 5,000 cash</li><li>Entries will be featured on a Belo digital billboard along EDSA (Guadalupe) from September 7-13, 2014.</li></ol>', '2015-04-10 05:29:02');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
