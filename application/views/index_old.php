<!DOCTYPE html>

<!--[if IE 8]><html class="ie8"><![endif]-->

<!--[if IE 9]><html class="ie9"><![endif]-->

<!--[if gt IE 9]><!--><html><!--<![endif]-->

<head>

    <title>Belo Essentials</title>	

    <meta charset="UTF-8" />    

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="description" content="Belo Essentials" />

    <meta name="keywords" content="belo essentials, essentials product, beauty product, cosmetic, body products, face product" />

    <!--[if lt IE 9]><script src="<?= base_url() ?>assets/js/vendor/html5.js"></script><![endif]-->

    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/main.css?v=1" />

    <script>

        (function(d, s, id) {

            var js, fjs = d.getElementsByTagName(s)[0];

            if (d.getElementById(id)) return;

            js = d.createElement(s);

            js.id = id;

            js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=596458210444542&version=v2.0";

            fjs.parentNode.insertBefore(js, fjs);

        }(document, 'script', 'facebook-jssdk'));

        function gAnalytics(category, action, label) {
            ga('send', {
                'hitType': 'event',
                'eventCategory': category,
                'eventAction': action,
                'eventLabel': label,
                'eventValue': '1'
            });
        }
    </script>

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-42684485-5', 'beloessentials.ph');
      ga('send', 'pageview');
    </script>

</head>



<body id="skrollr-body">



<header class="curvy"></header>

<header class="main">

    <div class="container">

        <h1 class="logo from-sprites"><a href="#">Belo Essentials Get Beautyful #ANNEderarms</a></h1>

        <a href="#" id="mininav">Mini Nav</a>

        <nav class="navigation">

            <a href="#home" id="home-sec" onclick="gAnalytics('Home', 'Home', 'Home')">Home</a>

            <a href="#mechanics" id="mech-sec" onclick="gAnalytics('Mechanics', 'Mechanics', 'Mechanics')"><span>Mechanics</span></a>

            <span class="logo-space">Logo Space</span>

            <a href="#prizes" id="prizes-sec" onclick="gAnalytics('Prizes', 'Prizes', 'Prizes')">Prizes</a>

            <a href="#product" id="prod-sec" onclick="gAnalytics('Products', 'Products', 'Products')"><span>Products</span></a>

        </nav>

        <div class="socials">

            <span>Beauty loves company,<br>so follow us online!</span>

            <a href="https://www.facebook.com/beloessentials" target="_blank" class="fb from-sprites fb-sharer">Facebook</a>

            <a href="https://twitter.com/Belo_Essentials" target="_blank" class="tw from-sprites tw-sharer">Twitter</a>

            <a href="https://instagram.com/belo_essentials" target="_blank" class="in from-sprites ig-sharer">Instagram</a>

        </div>

    </div>

    <button class="see-video" onclick="gAnalytics('Video', 'Click-Video', 'Click Video')">

        CLICK TO VIEW VIDEO

    </button>

</header>



<section class="main">

    <section id="home" class="clearfix" data-0="background-position:0px 0px;" data-100000="background-position:0px -50000px;">

        <div class="container">

            <div class="home-details">

                <div class="how-to">

                    <div class="ribbon">HOW TO JOIN</div>

                    <img src="<?= base_url() ?>assets/img/home-title.png">

                    <p><b>KiliKilig ka ba sa #ANNEderarms? Send in your KiliKilig pick-up lines!</b><br>You and nine others could each win P5,000 cash and an #ANNEderarm product kit. Plus, your entry could be displayed on a digital billboard in EDSA!
Show your KiliKilig love for #ANNEderarms. Join now!</p>



                    <br>

                    

                    <a href="https://twitter.com/Belo_Essentials" onclick="gAnalytics('Follow Belo', 'Follow-Belo', 'Follow Belo')" class="twitter-follow-button" data-show-count="false" data-lang="en"  data-size="large">Follow @Belo_Essentials</a>

                    

                    <br>

                    

                    <a href="#" class="btn btn-blue btn-tweet" onclick="twitter(); gAnalytics('Tweet', 'Tweet-to-Join', 'Tweet')"><span></span> TWEET TO JOIN</a>

                </div>

                <div class="tweet-feed">

                    <div class="ribbon">LIVE FEED TWEETS</div>

                    <ul id="tweet-start-feed">

                        <!-- TWEET FEEDS -->

                        <?php foreach( $entries as $k => $v ): ?>

                            <li>

                                <div class="thumb"><img src="<?php echo $v['user_image'] ?>"></div>

                                <span class="name"><?php echo $v['full_name'] ?> <em>@<?php echo $v['username'] ?></em></span>

                                <span class="date"><?php echo date('m/d/y', strtotime($v['created_at'])) ?></span>

                                <span class="tweet"><?php echo auto_hyper_link($v['caption']) ?></span>

                            </li>

                        <?php endforeach; ?>

                        <!-- END TWEET FEEDS -->

                    </ul>



                </div>



                <div class="clearfix"></div>

            </div>

        </div>

    </section>



    <section id="mechanics" class="clearfix" data-0="background-position:0px 0px;" data-100000="background-position:0px -50000px;">

        

        <div class="container">

         

            <div class="mechanics-details">

                <div class="left">

                    <img src="<?= base_url() ?>assets/img/twitter-logo.png"> <br><br>

                    <img src="<?= base_url() ?>assets/img/kilikiliglines.png">



                    <p>Say it loud, say it proud! Let the world know how much you love underarms. Join the #ANNEderarms #KiliKiligLines contest now and you could win some awesome prizes.</p>



                    <div class="text-center">

                        <img src="<?= base_url() ?>assets/img/beauty-deo.png"> 

                    </div>

                </div>

                

                <div class="right">

                    <ul class="caveman-mechanics">

                        <li>

                            <i>1</i>

                            <div class="copy">

                                <p>Follow @Belo_Essentials on Twitter.</p>



                                <a href="https://twitter.com/Belo_Essentials" onclick="gAnalytics('Follow Belo', 'Follow-Belo', 'Follow Belo')" class="twitter-follow-button" data-show-count="false" data-lang="en"  data-size="large">Follow @Belo_Essentials</a>



                            </div>

                        </li>



                        <li>

                            <i>2</i>

                            <div class="copy">

                                <p>Tweet your best kili-kili pick-up line. Don’t forget to use the hashtags #ANNEderarms and #KiliKiligLines, and tag @Belo_Essentials.</p>



                                <a href="#" class="btn btn-blue btn-tweet" onclick="twitter(); gAnalytics('Tweet', 'Tweet-to-Join', 'Tweet')"><span></span> TWEET TO JOIN</a>

                            </div>

                        </li>



                        <li>

                            <i>3</i>

                            <div class="copy">

                                <p>Wait for the announcement of winners on Facebook, Twitter, or Instagram.</p>



                                <a href="#" class="show_full_mechanics">VIEW FULL MECHANICS</a>

                            </div>

                        </li>

                    </ul>

                </div>



                <div class="clearfix"></div>

            </div>

                     

            

        </div>

    </section>

    

    <section id="prizes" class="clearfix" data-0="background-position:0px 0px;" data-100000="background-position:0px -50000px;">

        <div class="container">

            <h2>Ten kili-kili lovers will receive these exciting prizes. Be sure to follow Belo Essentials on Facebook, Twitter, or Instagram for the announcement of winners!</h2>

            <div class="content">



                <div class="sharer">

                    <button class="fb" onclick="sharer('fb'); gAnalytics('Go To Facebook', 'Go-To-Facebook', 'Go To Facebook')"></button>

                    <button class="tw" onclick="sharer('tw'); gAnalytics('Go To Twitter', 'Go-To-Twitter', 'Go To Twitter')"></button>

                    <button class="ig" onclick="sharer('ig'); gAnalytics('Go To Instagram', 'Go-To-Instagram', 'Go To Instagram')"></button>

                </div>

                

                <ul class="prizes clearfix">

                    <li>

                        <figure><img src="<?= base_url() ?>assets/img/prize-1.png" alt="3-day trip to Boracay for four"></figure>

                        Ten winning entries will be featured on a digital billboard along EDSA (Guadalupe) from September 7-13, 2014.

                    </li>

                    <li>

                        <figure><img src="<?= base_url() ?>assets/img/prize-2.png" alt="up to four Underarm Hair Removal sessions by Belo Medical Group"></figure>


                    </li>

                    <li>

                        <figure><img src="<?= base_url() ?>assets/img/prize-3.png" alt="one RevLite session by Belo Medical Group"></figure>

                        #ANNEderarm product kit

                    </li>

                </ul>



                

            </div>



            

        </div><!-- .container -->

    </section><!-- #prizes -->







    <section id="product" class="clearfix" data-0="background-position:0px 0px;" data-end="background-position:0px -5000px;">

        <div class="container">

            <h3>Get rid of these five common underarm problems for KiliKilig-worthy #ANNEderarms with Belo Beauty Deo!</h3>



            <ul class="skin">

                <li><img src="<?= base_url() ?>assets/img/skin-sweaty.png"> <span>SWEAT</span></li>

                <li><img src="<?= base_url() ?>assets/img/skin-rough.png"> <span>ROUGHNESS</span></li>

                <li><img src="<?= base_url() ?>assets/img/skin-chicken.png"> <span>CHICKEN SKIN</span></li>

                <li><img src="<?= base_url() ?>assets/img/skin-dark.png"> <span>DARKNESS</span></li>

                <li><img src="<?= base_url() ?>assets/img/skin-red.png"> <span>REDNESS</span></li>

            </ul>

            

            <div class="product-container">

                <div class="bubble-box">

                    Aptly called "The Beauty Deo", this product comes as a roll-on and quick-drying spray. With regular use, it rids you of the five most common underarm problems to give you absolutely beautiful, KiliKilig-worthy #ANNEderarms.

                </div>



                <div class="product-deo">

                    <div class="rollon">

                        Belo Essentials Whitening<br>

                        Deo Roll-On 40ml<br>

                        SRP: 89.75<br>

                        <a href="http://www.lazada.com.ph/belo-essentials-whitening-deo-roll-on-40ml-185900.html" onclick="gAnalytics('Buy Now Roll-On', 'Buy-Now-Roll-On', 'Buy Now Roll-On')" target="_blank" class="btn btn-default btn-medium">BUY NOW</a>

                    </div>

                    <div class="can"></div>

                    <div class="spray">

                        Belo Essentials Whitening<br>

                        Deo Spray 140ml<br>

                        SRP: 179.75<br>

                        <a href="http://www.lazada.com.ph/belo-essentials-whitening-deo-spray-140ml-185901.html" onclick="gAnalytics('Buy Now Spray', 'Buy-Now-Spray', 'Buy Now Spray')" target="_blank" class="btn btn-default btn-medium">BUY NOW</a>

                    </div>

                </div>

            </div>

        </div><!-- .container -->

    </section><!-- #product -->

</section><!-- .main content -->



<footer class="main">

    <div class="container">

        <div class="left clearfix">

            <h1 class="logo from-sprites"><a href="#">Belo Essentials Get Beautyful #ANNEderarms</a></h1>



            <div class="footnav">

                <a href="#home">HOME</a>&nbsp;&nbsp;|&nbsp;&nbsp;

                <a href="#prizes">PRIZES</a>&nbsp;&nbsp;|&nbsp;&nbsp;

                <a href="#mechanics">MECHANICS</a>&nbsp;&nbsp;|&nbsp;&nbsp;

                <a href="#product">PRODUCTS</a>

                <p class="copyright">Copyright © 2014 Belo Essentials. All rights reserved.</p>

            </div>



            <p>

                <a href="#" class="share-fb from-sprites" onclick="facebook(); gAnalytics('Share Website Facebook', 'Share-Website-Facebook', 'Share Website Facebook')">Share with Facebook</a>

                <a href="#" class="share-tw from-sprites" onclick="share_twitter(); gAnalytics('Share Website Twitter', 'Share-Website-Twitter', 'Share Website Twitter')">Share with Twitter</a>

            </p>

        </div>

        <div class="right clearfix">

            <figure class="footer-product">Belo Deo</figure>

        </div>

    </div>

</footer><!-- .main footer -->


<script type="text/javascript"> var siteUrl = '<?php echo site_url()?>'; var baseUrl = '<?php echo base_url()?>'; </script>

<script>window.jQuery || document.write('<script src="<?= base_url() ?>assets/js/vendor/jquery-1.10.2.min.js"><\/script>')</script>

<!--[if gt IE 8]><!--><script type="text/javascript" src="<?= base_url() ?>assets/js/vendor/skrollr.min.js"></script><!--<![endif]-->

<script type="text/javascript" src="<?= base_url() ?>assets/js/vendor/jquery.fitvids.js"></script>

<script type="text/javascript" src="<?= base_url() ?>assets/js/vendor/jquery-ui.js"></script>

<script type="text/javascript" src="<?= base_url() ?>assets/js/vendor/jquery.slimscroll.min.js"></script> 

<script type="text/javascript" src="<?= base_url() ?>assets/js/vendor/ScrollAnalytics.js"></script>                       

<script type="text/javascript" src="<?= base_url() ?>assets/js/main.js?v=1&r=<?=rand()?>"></script>

<script type="text/javascript" src="<?= base_url() ?>assets/js/myJs.js"></script>





 <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

 <script>
    $("a#home-sec").on("click", function(){
         ga('send', 'pageview', 'home');
    });

    $("a#mech-sec").on("click", function(){
         ga('send', 'pageview', 'mechanics');
    });

    $("a#prizes-sec").on("click", function(){
         ga('send', 'pageview', 'prizes');
    });

    $("a#prod-sec").on("click", function(){
         ga('send', 'pageview', 'products');
    });
 </script>



<!-- popup -->

<div id="popup-full-mechanics">

    <div class="popup">

        <div class="popup-content for-mechanics">

            <div class="content clearfix">

                <a href="#" class="close">Close Popup</a>



                <div class="slider">

                    <div class="padding">

                    <h3>Full Mechanics</h3>

                        <?php echo $mechanics[0]['content'] ?>

                    </div><!-- .padding -->

                </div><!-- .slider -->

            </div>

        </div><!-- .popup-conten -->

    </div><!-- .popup -->

    <div class="blur"></div>

</div>

<!-- .popup -->



<!-- popup -->

<div id="popup-video">

    <div class="popup">

        <div class="popup-content for-upload">

            <a href="#" class="close">CLOSE</a>

            <iframe width="100%" height="100%" src="//www.youtube.com/embed/cBdj2NSC8ho?rel=0" frameborder="0" allowfullscreen></iframe>

        </div>

    </div>

    <div class="blur"></div>

</div>

<!-- .popup -->

</body>

</html>