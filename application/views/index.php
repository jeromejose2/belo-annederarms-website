<!DOCTYPE html>
<!--[if IE 8]><html class="ie8"><![endif]-->
<!--[if IE 9]><html class="ie9"><![endif]-->
<!--[if gt IE 9]><!--><html><!--<![endif]-->
<html prefix="og: http://ogp.me/ns#" lang="en">
<head>
    <title>Belo Essentials</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Belo Essentials" />
    <meta name="keywords" content="belo essentials, essentials product, beauty product, cosmetic, body products, face product" />
    <link rel="stylesheet" href="assets/vendors/font-awesome/css/font-awesome.min.css">
    <!--[if lt IE 9]><script src="<?= base_url() ?>js/vendor/html5.js"></script><![endif]-->
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/main.min.css">
    <link rel="shortcut icon" type="image/x-icon" href="<?=base_url()?>assets/img/favicon.jpg" />
    <meta property="og:title" content="Be a wANNEderwoman of your own!" />
    <meta property="og:type" content="With the BeloBeautyDuo, you too can have wonderful ANNEderarms" />
    <meta property="og:url" content="<?=base_url() ?>" />
</head>

<body>



<header class="curvy"></header>
<header class="main">
    <div class="container">
        <h1 class="logo from-sprites"><a href="#">Belo Essentials Get Beautyful #ANNEderarms</a></h1>
        <a href="#" id="mininav">Mini Nav</a>
        <nav class="navigation">
            <a href="#home" >Home</a>
            <a href="#mechanics"><span>Mechanics</span></a>
            <span class="logo-space">Logo Space</span>
            <a href="#prizes">Prizes</a>
            <a href="#product"><span>Products</span></a>
        </nav>
        <div class="socials">
            <span>Beauty loves company,<br>so follow us online!</span>
            <a href="https://www.facebook.com/beloessentials" target="_blank" class="fb from-sprites">Facebook</a>
            <a href="https://twitter.com/Belo_Essentials" target="_blank" class="tw from-sprites">Twitter</a>
            <a href="https://instagram.com/belo_essentials" target="_blank" class="in from-sprites">Instagram</a>
        </div>
    </div>
    <button class="see-video" data-video-url="https://www.youtube.com/embed/2mQPYzCN5sU?rel=0">
        Watch Anne's ANNEderarm moves here
    </button>
</header>

    <section class="main">
        <section id="home" class="light-bg clearfix">
            <div class="container">
                <div class="home-details">
                    <div class="how-to">
                        <div class="ribbon">HOW TO JOIN</div>
                        <img src="assets/img/anne-home.png">
                        <h1 class="hashtag">#ANNE<span>DERARMS</span></h1>
                        <p>
                            Like Anne, you can fight darkness with the <strong>#BeloBeautyDuo</strong>!
                            Simply send us your best wANNEderwomanpose on Instagram, and you can win a <strong>#BeloBeautyDuo</strong> Kit PLUS a trip to Boracay!
                        </p>
                        <h2 class="komika">Join now!</h2>
                    </div>
                    <div class="tweet-feed">
                        <div class="ribbon">PHOTO ENTRIES</div>

                        <div class="infinite-scroll">
                            <ul class="people-lists">
                                <?php foreach( $entries as $k => $v ): ?>
                                <li style="background-image:url(<?php echo $v['image_thumbnail'] ?>)" data-url="<?php echo $v['image_url'] ?>" data-username="<?php echo $v['username'] ?>" data-desc="<?php echo $v['caption'] ?>" data-large="<?php echo $v['image_standard'] ?>" data-fullname="<?php echo $v['full_name'] ?>"></li>
                                <?php endforeach; ?>

                                <div class="picture-cont">
                                    <div class="btn-close"></div>
                                    <div class="picture-info">
                                                <ul class="people-details">
                                                    <li> <span class="username"><i class="fa fa-instagram"></i></span></li>
                                                    <li> <span class="fullname"></span></li>
                                                </ul>
                                                <div class="clearfix"></div>

                                                <span class="desc"></span>

                                               <div class="share-this clearfix">
                                                    <ul>
                                                        <li><a href="#" class="share-facebook">lorem</a></li>
                                                        <li><a href="#" class="share-twitter">lorem</a></li>
                                                    </ul>
                                               </div>
                                    </div>
                                </div>
                            </ul>
                        </div>

                        <div class="search">
                            <!--<label for="data[address][0]">Address 1</label>-->
                                <input placeholder="Type your name to search your entry" type="text" name="search" class="search-feed">
                                <button><i class="fa fa-search"></i></button>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>
        </section>

        <section id="mechanics" class="light-bg clearfix" >

            <div class="container">

                <div class="mechanics-details">
                    <div class="left">
                        <!--<img src="<?= base_url() ?>assets/img/twitter-logo.png">-->
                        <h1 class="hashtag">#BELOBEAUTYDuO</h1>
                        <h1 class="hashtag">#ANNE<span>DERARMS</span></h1>

                        <p>Join us in our mission of banishing dark underarms and win some awesome prizes to boot!</p>

                        <div class="text-center">
                            <img src="<?= base_url() ?>assets/img/beauty-deo.png">
                        </div>
                    </div>

                    <div class="right">
                        <ul class="caveman-mechanics">
                            <li>
                                <i>1</i>
                                <div class="copy">
                                    <p>Take a photo of your <strong>wANNEderwoman</strong> pose. </p>


                                </div>
                            </li>

                            <li>
                                <i>2</i>
                                <div class="copy">
                                    <p>
                                        Upload to Instagram with the hashtags
                                        <b>#BeloBeautyDuo</b> and <b>#ANNEderarms</b>.
                                    </p>


                                </div>
                            </li>

                            <li>
                                <i>3</i>
                                <div class="copy">
                                        <p>
                                            Tag <b>@belo_essentials</b> and
                                            you’re good to go!
                                        </p>
                                        <div class="mechanic-cont">
                                             <a href="#" class="btn show_full_mechanics"><i class="fa fa-arrow-right"></i> VIEW FULL MECHANICS</a>
                                        </div>

                                </div>
                            </li>

                        </ul>
                         <div class="disclaimer">
                             <p>
                                *Set your Instagram accounts to public so we can see your entries!
                            </p>
                            <p>
                              *Contest runs from April 18 – May 17, 2015.
                             </p>
                         </div>
                    </div>

                    <div class="clearfix"></div>
                </div>


            </div>
        </section>

        <section id="prizes" class="dark-bg clearfix" >
            <div class="container">
                <div class="title">
                    <h4>
                            5 weekly winners get a special <strong>#ANNEderarms Kit</strong>.
                     </h4>
                     <p>
                         The   <b>grand prize winner</b> will receive the following:
                     </p>
                </div>
                <div class="content">



                    <ul class="prizes clearfix">
                        <li>
                            <figure><img src="<?= base_url() ?>assets/img/prize-1.png" alt="3-day trip to Boracay for four"></figure>
                            4 underarm RevLite sessions
                            from the Belo Medical Group
                        </li>
                        <li>
                            <figure><img src="<?= base_url() ?>assets/img/prize-2.png" alt="up to four Underarm Hair Removal sessions by Belo Medical Group"></figure>
                            <strong>#ANNEderarms Kit</strong>
                        </li>
                        <li>
                            <figure><img src="<?= base_url() ?>assets/img/prize-3.png" alt="one RevLite session by Belo Medical Group"></figure>
                            3 day/2 night trip for 4
                            to BORACAY!
                        </li>
                    </ul>


                </div>


            </div><!-- .container -->
        </section><!-- #prizes -->



        <section id="product" class="light-bg clearfix" >
            <div class="container">
                <h3>Say good bye to your underarm problems with Belo Essentials Beauty Deo!</h3>

                <ul class="skin">
                    <li><img src="<?= base_url() ?>assets/img/skin-sweaty.png"> <span>SWEATY</span></li>
                    <li><img src="<?= base_url() ?>assets/img/skin-rough.png"> <span>ROUGH</span></li>
                    <li><img src="<?= base_url() ?>assets/img/skin-chicken.png"> <span>CHICKEN SKIN</span></li>
                    <li><img src="<?= base_url() ?>assets/img/skin-dark.png"> <span>DARK</span></li>
                    <li><img src="<?= base_url() ?>assets/img/skin-red.png"> <span>RED</span></li>
                </ul>

                <div class="product-container">
                    <div class="bubble-box">
                      Fighting underarm darkness? Power-up with the <strong>#BeloBeautyDuo</strong>! Use the Whitening Anti-Perspirant Deodorant by day for smoother skin and minimized pores. Then apply the Whitening Cream at night to whiten skin at the cellular level. Deo by day and the Whitening Cream by night – banish dark underarms with the <strong>#BeloBeautyDuo</strong>!
                    </div>
                    <div class="clearfix"></div>
                   <div class="products">
                             <div class="clearfix hashtag-img">
                                 <img src="<?= base_url() ?>assets/img/comics.png"/>
                             </div>
                            <div class=" per-product">
                                        <div class="product-img p1">

                                        </div>
                                        <div class="per-details">
                                                        <div class="details-copy">
                                                                    <span>
                                                                        Underarm<br/>
                                                                        Whitening<br/>
                                                                        Cream (40g)<br/>
                                                                        <b>SRP: 349.75</b><br/>
                                                                    </span><br/>
                                                                    <a data-url="http://www.zalora.com.ph/Essentials-Underarm-Whitening-Cream-40g-252038.html" class="btn btn-default btn-medium">BUY NOW</a>
                                                        </div>
                                        </div>
                            </div>
                              <div class=" per-product">
                                        <div class="product-img p2">

                                        </div>
                                        <div class="per-details">
                                                         <div class="details-copy">
                                                                    <span>
                                                                        Whitening<br/>
                                                                        Anti-Perspirant Deodorant<br/>
                                                                        Original Scent Roll on (40mL)<br/>
                                                                        <b>SRP: 92.75</b><br/>
                                                                    </span><br/>
                                                                    <a data-url="http://www.lazada.com.ph/belo-essentials-whitening-deo-roll-on-40ml-set-of-2-with-free-belo-underarm-whitening-cream-10ml-250449.html?mp=1" class="btn btn-default btn-medium">BUY NOW</a>
                                                        </div>
                                        </div>
                            </div>
                              <div class=" per-product">
                                        <div class="product-img p3">

                                        </div>
                                        <div class="per-details">
                                                             <div class="details-copy">
                                                                    <span>
                                                                        Whitening<br/>
                                                                        Anti-Perspirant Deodorant<br/>
                                                                        Shower Fresh Scent Roll on (40mL)<br/>
                                                                        <b>SRP: 92.75</b><br/>
                                                                    </span><br/>
                                                                    <a data-url="http://www.lazada.com.ph/belo-essentials-whitening-anti-perspirant-deodorant-shower-fresh-roll-on-40ml-347451.html?mp=1" class="btn btn-default btn-medium">BUY NOW</a>
                                                        </div>
                                        </div>
                            </div>
                              <div class=" per-product">
                                        <div class="product-img p4">

                                        </div>
                                        <div class="per-details">
                                                         <div class="details-copy">
                                                                    <span>
                                                                        Whitening<br/>
                                                                        Anti-Perspirant Deodorant<br/>
                                                                        Original Scent (140mL)<br/>
                                                                        <b>SRP: 184.75</b><br/>
                                                                    </span><br/>
                                                                    <a data-url="http://www.lazada.com.ph/belo-essentials-whitening-anti-perspirant-deodorant-spray-140ml-with-free-belo-underarm-whitening-cream-10ml-185901.html?mp=1" class="btn btn-default btn-medium">BUY NOW</a>
                                                        </div>
                                        </div>
                            </div>
                              <div class=" per-product">
                                        <div class="product-img p5">

                                        </div>
                                        <div class="per-details">
                                                         <div class="details-copy">
                                                                    <span>
                                                                        Whitening<br/>
                                                                        Anti-Perspirant Deodorant<br/>
                                                                        Shower Fresh Scent Spray (140mL)<br/>
                                                                        <b>SRP: 184.75</b><br/>
                                                                    </span><br/>
                                                                    <a data-url="http://www.lazada.com.ph/belo-essentials-whitening-anti-perspirant-deodorant-shower-fresh-spray-140ml-347454.html?mp=1" class="btn btn-default btn-medium">BUY NOW</a>
                                                        </div>
                                        </div>
                            </div>

                    </div>
                </div>
            </div><!-- .container -->
        </section><!-- #product -->
    </section><!-- .main content -->

<footer class="main">
    <div class="container">
        <div class="left clearfix">
            <h1 class="logo from-sprites"><a href="#">Belo Essentials Get Beautyful #ANNEderarms</a></h1>

            <div class="footnav">
                <a href="#home">HOME</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="#prizes">PRIZES</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="#mechanics">MECHANICS</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="#product">PRODUCTS</a>
                <p class="copyright">Copyright © 2015 Belo Essentials. All rights reserved.</p>
            </div>

            <p>
                <a target="_blank" href="http://www.facebook.com/sharer/sharer.php?s=100&amp;p[url]=<?=base_url()?>&amp;p[title]=Be a wANNEderwoman of your own!&amp;p[summary]=With the BeloBeautyDuo, you too can have wonderful ANNEderarms. Click here to know more!"  class="share-fb from-sprites">Share with Facebook</a>
                <a href="javascript:void(0)"  onClick="postToTwitter();" class="share-tw from-sprites">Share with Twitter</a>
            </p>
        </div>
        <div class="right clearfix">
            <figure class="footer-product">Belo Deo</figure>
        </div>
    </div>
</footer><!-- .main footer -->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-42684485-31', 'auto');
  ga('send', 'pageview');
</script>


<script>
    window.jQuery || document.write('<script src="assets/vendors/jquery-1.10.2.min.js"><\/script>')
</script>
<!--[if gt IE 8]><!-->
<!--<![endif]-->
<script type="text/javascript" src="<?= base_url() ?>assets/vendors/jquery.fitvids.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/vendors/jquery-ui.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/vendors/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/vendors/jquery.jscroll.min.js"></script>
<script src="<?= base_url() ?>assets/js/main.min.js"></script>
<script src="<?= base_url() ?>assets/js/extend.js"></script>


 <script>
 window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));

 var feedCtrl = {
    request: null,
    noData: false,
    offset: $('.tweet-feed .people-lists > li').length,
    queryString: '',
    scrollTop: false,

    popup: function() {
        $('.tweet-feed .people-lists > li').on('click',function(){
            var isLoaded =  $('.picture-cont').hasClass('active');
            var imgSrc = $(this).data('large');
            var  username = $(this).data('username');
            var  fullname = $(this).data('fullname');
            var desc = $(this).data('desc');
            var url = $(this).data('url');
            var el = $('.picture-cont');
            el.css({
                'background-image':'url('+imgSrc+') '
            })
            $('span.username').html(' <i class="fa fa-instagram"></i> '+ username +'');
            $('span.fullname').html(fullname);
            $('span.desc').html(desc);
            $('a.share-twitter').attr('href', 'https://twitter.com/share?url=' + url + '&text=' + encodeURIComponent('#ANNEderarms #BeloBeautyDuo'));
            $('a.share-facebook').attr('href', "https://www.facebook.com/sharer/sharer.php?u=" + url);

            if(isLoaded){
                 el.removeClass('active');
                 setTimeout(function(){
                     el.addClass('active');
                 },601)
            }else{
                el.addClass('active');
            }

        });
    },

    getJson: function() {
        $('.infinite-scroll').append('<p align="center" class="loading"><img src="' + '<?= base_url() ?>' + 'assets/admin/img/spinner.gif"></p>');

        feedCtrl.request = $.getJSON( "<?= base_url() ?>" + "home/feed/" + feedCtrl.offset + '?query=' + encodeURIComponent( feedCtrl.queryString ), function( response ) {
            $('.infinite-scroll').find( '.loading').delay(800).queue(function() {
                $(this).remove();

                var content = '';
                if ( response !== undefined && !$.isEmptyObject( response ) ) {
                    $.each( response.entries, function(index, value) {
                        var image = '<li style="background-image:url(' + value.image_thumbnail + ')" data-url="' + value.image_url + '" data-username="' + value.username + '" data-desc="' + value.caption + '" data-large="' + value.image_standard + '" data-fullname="' + value.full_name + '"></li>';
                        content += image;
                    });

                    feedCtrl.offset = response.offset;

                    if ( response.dataCount == 0 ) {
                        feedCtrl.noData = true;
                    }
                }

                if ( feedCtrl.scrollTop == true ) {
                    $('.infinite-scroll').slimScroll({ 'scrollTo' : '0px' });
                    feedCtrl.scrollTop = false;
                }

                $('.tweet-feed .people-lists').append( $( content ) );
                feedCtrl.popup();
            });
        }).done( function() {
            feedCtrl.request = null;
        });
    },

    init: function() {
        feedCtrl.popup();

        $('.search button').click( function(){
            feedCtrl.queryString = $('.search input[name=search]').val();
            feedCtrl.offset = 0;
            feedCtrl.noData = false;
            feedCtrl.scrollTop = true;

            if ( feedCtrl.request ) {
                feedCtrl.request.abort();
                feedCtrl.request = null;
            }

            $('.tweet-feed .people-lists > li').remove();
            feedCtrl.getJson();
        });

        $('.infinite-scroll').slimScroll({
            height:'auto',
            alwaysVisible: true,
            wheelStep: 10

        }).bind('slimscroll', function(e, pos) {
            if (pos == 'bottom') {
                if ( feedCtrl.request == null && feedCtrl.noData == false ) {
                    feedCtrl.getJson();
                }
            }

        });
    }
 };

$(function(){
    var scrWidth = $(window).width();
    var slimheight = '550px';
    if(scrWidth <= 767){
         slimheight = '700px';
    }

    console.log(slimheight);
    $('.slider').slimScroll({
            height:slimheight,
            alwaysVisible: true
    });

    //infinite scrolling
    feedCtrl.init();

    <?php if (isset($end_message)): ?>
 		$('#popup-end-message').show();
 	<?php endif ?>

    /* FB & TWITTER SHARE BUTTON */
    $('.share-facebook, .share-twitter').click(function() {
        var url = $(this).attr('href');
        var popup = window.open(url, "pop", "width=600, height=400, scrollbars=no");
    });

   $('.btn-close').on('click',function(){
             $('.picture-cont').removeClass('active')
   })



});
 </script>
 <script type="text/javascript">

    function postToTimeline() {
        FB.ui({
              method: 'feed',
              name: '3 Causes For Christmas',
              link: '<?=base_url()?>',
              picture: 'http://nwshare.ph/bountyfresh/likevocacy/fb/images/share-app.jpg',
              caption: 'lorem',
              description: 'Click your support this holiday season and Bounty Fresh will donate to a worthy cause! Click now! http://on.fb.me/TCnFDR '
             },function (response) {
               }
            );
    }

    function postToTwitter() {
        twitterwindow = window.open('http://twitter.com/?status='+encodeURIComponent('The #BeloBeautyDuo has arrived. Click here and have beautiful #ANNEderarms <?=base_url()?>'),'Share','width=400,height=300');

        twitterwindow.moveTo(300,300);
    }

</script>

<!-- mechanics popup -->
<div id="popup-full-mechanics">
    <div class="popup">
        <div class="popup-content for-mechanics">
            <div class="content clearfix">
                <a href="#" class="close">Close Popup</a>

                <div class="slider">
                    <div class="padding">
                    <h3>Full Mechanics</h3>

                        <?= $mechanics[0]['content'] ?>

                    </div><!-- .padding -->
                </div><!-- .slider -->
            </div>
        </div><!-- .popup-conten -->
    </div><!-- .popup -->
    <div class="blur"></div>
</div>
<!-- .popup -->

<?php if (isset($end_message)): ?>
<!-- end message popup -->
<div id="popup-end-message">
    <div class="popup">
        <div class="popup-content for-end-message">
            <div class="content clearfix">
                <a href="#" class="close">Close Popup</a>

                <div class="slider">
                    <div class="padding">
                    <h3>End of Promo</h3>
                        <?= $end_message ?>
                    </div><!-- .padding -->
                </div><!-- .slider -->
            </div>
        </div><!-- .popup-conten -->
    </div><!-- .popup -->
    <div class="blur"></div>
</div>
<!-- .popup -->
<?php endif ?>

<!-- video popup -->
<div id="popup-video">
    <div class="popup">
        <div class="popup-content for-upload">
            <a href="#" class="close">CLOSE</a>
            <iframe width="100%" height="100%" src="" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
    <div class="blur"></div>
</div>
<!-- .popup -->
</body>
</html>

