<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()	{

		$params = array(
			'table'=>'tbl_entries',
			'where'=>'status = 1',
			'order'=>'created_at DESC',
			'limit' => '21',
			'offset' => '0'
		);
		$this->data['entries'] = $this->mysql_queries->get_data($params);

		$params = array(
			'table'=>'tbl_settings',
			'where'=>'type = \'mechanics\''
		);
		$this->data['mechanics'] = $this->mysql_queries->get_data($params);
		
		//get promo duration
		$params = array(
			'table'=> 'tbl_promo_duration',
			'fields' => 'end_date, end_message'
		);
		$promo_duration = $this->mysql_queries->get_data($params);
		
		//compare today and end date
		$today_date = strtotime(date('Y-m-d'));
		$end_date = strtotime($promo_duration[0]['end_date']);
		if ($today_date >= $end_date) {
			$this->data['end_message'] = $promo_duration[0]['end_message'];
		}

		function auto_hyper_link($string) {
			$break = explode(" ", $string);
			// replacing process
			for ($i=0; $i<=sizeof($break)-1; $i++) {
				if ((substr($break[$i], 0, 7) == 'http://') && ($break[$i] != 'http://')) {
					$latest_post = str_replace($break[$i], "<a target='_blank' href='".$break[$i]."'>".$break[$i]."</a>", $string);
				}
			}

			if(!empty($latest_post)) {
				return $latest_post;
			} else {
				return $string;
			}
		}

		$this->load->view('index', $this->data);

	}
	
	public function feed($offset)
	{
		$params = array(
			'offset' => $offset,
			'limit' => 21,
			'queryString' => $_GET['query']
		);
		
		$this->data['entries'] = $this->mysql_queries->get_feed($params);
		$this->data['dataCount'] = count($this->data['entries']);
		$this->data['offset'] = $offset + $this->data['dataCount'];
		
		$this->output->set_content_type('application/json')->set_output(json_encode($this->data));
	}

	public function foobar() {
		echo "Working!";
	}
    
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
