<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()	{

		$params = array(
			'table'=>'tbl_entries',
			'where'=>'status = 1',
			'order'=>'timestamp DESC'
		);
		$this->data['entries'] = $this->mysql_queries->get_data($params);

		$params = array(
			'table'=>'tbl_settings',
			'where'=>'type = \'mechanics\''
		);
		$this->data['mechanics'] = $this->mysql_queries->get_data($params);

		function auto_hyper_link($string) {
			$break = explode(" ", $string);
			// replacing process
			for ($i=0; $i<=sizeof($break)-1; $i++) {
				if ((substr($break[$i], 0, 7) == 'http://') && ($break[$i] != 'http://')) {
					$latest_post = str_replace($break[$i], "<a target='_blank' href='".$break[$i]."'>".$break[$i]."</a>", $string);
				}
			}

			if(!empty($latest_post)) {
				return $latest_post;
			} else {
				return $string;
			}
		}

		$this->load->view('index', $this->data);

	}
    
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */